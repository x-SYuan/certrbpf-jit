# End-to-end Mechanized Proof of a JIT-accelerated eBPF Virtual Machine for IoT

This repository contains the version of the verified JIT + HAVM presented in the CAV24 submission 1579.
  
## Installation
see [install](INSTALL.md). 

We provide two log files `compile_dx_log.txt` (compiling dx on tested Ubuntu OS) and `compile_make_all_log.txt`(compiling this project on tested Ubuntu OS) to help users to check if they can install this project correctly.

## Link to paper
The relation between our CAV24 paper and this coq development. See [linktopaper](LINKTOPAPER.md).

We also provide a makefile command to generate html files (see [html](html)) in order to help users to read the coq development without executing Coq code (it uses the [coq2html](https://github.com/xavierleroy/coq2html) tool)

```shell
# install coq2html if you hope to generate CertrBPF-JIT Coq development document with the html form
git clone https://github.com/xavierleroy/coq2html.git
cd coq2html
make coq2html

cd YOUR-CertrBPF-JIT-DIR/
# assume you has compiled the certrbpf-jit project succssfully
make document
```

## Building for the Nordic nRF52DK

The following benchmark requires a phycial Nordic nRF52DK developing board and a specific variant of RIOT-OS:
- [nRF52840DK](https://www.digikey.com/en/products/detail/nordic-semiconductor-asa/NRF52840-DK/8593726).
- [RIOT-OS](benchmark_data/RIOT).

_NB:The actual result (Table 1 in the paper) may have slight difference because of difference nRF52DK boards, BUT the conclusion presented in the paper is always true_

The benchmark applications are available in the `benchmark_data` directory. It
contains the main RIOT tree as used to produce the results and the eight benchmark
applications.

The benchmark applications are automated. They will execute on the targeted
platform and output the benchmark results over the available serial console
after running the benchmark.

To build the example applications, a toolchain is required. See the included
[RIOT documentation](benchmark_data/RIOT/doc/doxygen/src/getting-started.md) for
the exact tools required for each target platform.

Now, we explain how to evaluate the benchmarks. Please open two terminal windows:
- A: showing the experiment output
```Console
$ YOUR_DIR/benchmark_data/RIOT/dist/tools/pyterm/pyterm -p "/dev/ttyACM0" -b "115200"
```
- B: Compiling the applications for the Nordic nRF52DK development kit can be done with
```Console
$ cd YOUR_DIR
$ export BOARD=nrf52dk
$ make test_bench
# BPF_COQ= 0 is vanilla-rBPF, = 1 is CertrBPF, = 2 is HAVM
```
We provide a makefile command `test_bench` to test eight benchmarks (`incr`, `square`, `bitswap`, `fib`, `sock_buf`, `memcpy`, `fletcher32`, `bsort`), for each benchmark, we run it three times (for averge) on each rBPF VM, so the first three outputs in terminal A are the execution time records that executing `incr` on vanilla-rBPF three times, etc.

The command flashes the application code on the board and will start a serial console
attached to the serial interface of the board.

NB: **We provide the [raw result](/benchmark_data/raw_result/havm_log.tex) (date: 2024-01-18) of HAVM performance on the benchmarks, each is tested by three times. We redo the [above benchmarks](/benchmark_data/raw_result/2024-04-21) and provide the latest result on the same folder.**

## JIT Overview

JIT consists of:
- An analyzer to find all rBPF Alu lists from the input rBPF program `rbpf32/Analyzer.v`;
- A JIT_ALU Compiler that translates a list of rBPF Alu into ARM binary `jit/ThumbJIT.v`;
- A Combiner that calls the JIT_ALU compiler to jit all rBPF Alu lists found by the analyzer `jit/WholeCompiler.v`.

The high-level specifiation includes three modules for proof simplification, the low-level C implementation adopts a more-efficient way that combines all modules togethers (the C function `whole_compiler` in `jit/jitclight/rbpf_jit_compiler.c`): find an Alu, then jit it so that there are less intermediate results.

## HAVM Overview

HAVM consists of:
- a `jit_call` function uses ARM hardware directly to execute jited code.
- a 32-bit rBPF interpreter executes the rest rBPF Non-Alu instructions, with defensive semantics.

## Project Structure
- `compcert-arm-bin.zip`: our symbolic CompCert ARM interpreter `compcert/arm/ABinSem.v`
- `rbpf32/`: high-level specification: i.e. transition systems of rbpf32 `rbpf32/TS*.v`
- `jit/`: the JIT compiler `jit/ThumbJIT.v` + HAVM `jit/TSSemanticsB.v`
