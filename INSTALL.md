This file is used to explain how to build CertrBPF-JIT step-by-step.

Please don't hesitate to contact [me](https://shenghaoyuan.github.io/) as the install is not easy.

_NB_
- we only test it on Ubuntu 20.04 (Fedora is also probably OK, not sure)
- to build RIOT, please check the [RIOT documentation](benchmark_data/RIOT/doc/doxygen/src/getting-started.md), [RIOT-OS offical website](https://www.riot-os.org/) or [RIOT-OS forum](https://forum.riot-os.org/)
- _you may need to add `sudo` to install some software_


# Build world: opam

1. basic enviroment for experiments (you can skip it)
```shell
# assume your ubuntu is named `bob`
# install the basic tool
apt-get update && apt-get install -y software-properties-common

# install gcc 9.4.0
apt install build-essential

# install gcc lib
apt-get install gcc-multilib

# install llvm
apt-get install -y llvm
apt-get install clang

# install pip3 and pyelftools
apt install python3-pip python3-serial
pip3 install pyelftools
```

2. OPAM
```shell
# install opam
add-apt-repository ppa:avsm/ppa
apt update
apt install opam

# install ocaml+coq by opam
opam init
# install ocaml
opam switch create bpf ocaml-base-compiler.4.11.1

eval $(opam env)

# It is better to restart your computer/VM to flash the enviroment, Important !!!

opam switch list
#   switch  compiler      description
->  bpf     ocaml.4.11.1  bpf

# Once you get a warning, please do `eval $(opam env)` and restart your computer/VM

# make sure your ocaml is 4.11.1 and from `/home/bob/.opam/bpf/bin/ocaml`
which ocaml

# install coq, etc.
opam repository add coq-released https://coq.inria.fr/opam/released

# install coq etc (optional: and coqide)
opam install coq.8.13.2 coq-elpi.1.11.0 coqide.8.13.2

# optional: install headache
# `opam install depext`
# `opam depext headache`
# `opam install headache`

```
# Build CompCertBin-ARM
```shell
# compcert-arm-bin.zip can be found in the root folder
cp compcert-arm-bin.zip YOUR_DIR/
cd YOUR_DIR/
unzip compcert-arm-bin.zip -d CompCertBin/

# install coq-flocq.4.1.0
opam install coq-flocq.4.1.0

# you may need a old menhir
opam install menhir.20210419

# install CompCertBin-ARM
cd YOUR_DIR/CompCertBin/compcert/
./configure arm-linux -use-external-Flocq -clightgen
make all # It will return some errors because of the lack of linkers when your architecture is not ARM, but it is OK
make clightgen

# set COQPATH
# Important: if you recompile CompCert again, remember to comment COQPATH firstly!!!
vim /home/bob/.bashrc
# adding the line `export COQPATH="YOUR_DIR/CompCertBin"`
source /home/bob/.bashrc
```

## Install dx

```shell
# Check CompCert PATH is OK, it should return `YOUR_DIR/CompCertBin`
# The COQPATH is not the path to COQ but to CompCert, because dx requires such as specific environment path name
echo $COQPATH

# Check CompCert compiler is OK, it should return `The CompCert C verified compiler, version 3.12`
YOUR_DIR/CompCertBin/compcert/ccomp --version

# Check CompCert clightgen tool is OK, it should return `The CompCert CompCert AST generator, version 3.12`
YOUR_DIR/CompCertBin/compcert/clightgen --version

# install dx:
git clone https://gitlab.univ-lille.fr/samuel.hym/dx.git
cd dx
./configure --cprinterdir="$(opam var lib)/dx" --compcertdir=YOUR_DIR/CompCertBin/compcert --install-compcert-printer
make install all
```

Please check if your terminal outputs the same result as `compile_dx_log.txt`.
_NB1: once you compile dx and return some error, you should always do `make clean` before recompiling dx_

# Build CertrBPF-JIT

```shell
# if you did not clone this project, you could use the following git command
# git clone --branch CAV24-AE https://gitlab.inria.fr/x-SYuan/certrbpf-jit.git

# go to the root folder of this project
cd certrbpf-jit

# The current Makefile works on Ubuntu, but we do find that `make all` returns an error (`Unknown option \n`) in Fedora. So if you are using Fedora, please remove the `"\n"` from the Makefile.

# do makefile configuration and generate _CoqProject
./configure --opamprefixdir=$OPAM_SWITCH_PREFIX --compcertdir=$COQPATH/compcert

make all # it may take 3-5 mins, take a cup of coffee (The compilation should not output any warnings)
```
