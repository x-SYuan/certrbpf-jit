From compcert.lib Require Import Integers Coqlib.
From compcert.common Require Import AST Values Memory.
From compcert.arm Require Import ABinSem BinDecode.

From bpf.comm Require Import ListAsArray LemmaNat JITCall.
From bpf.rbpf32 Require Import JITConfig TSSyntax TSDecode Analyzer.
From bpf.jit Require Import ThumbJIT1 ListSetRefinement ThumbJIT1proof ListSetSort.
From bpf.jit Require Import ThumbJIT2 WholeCompiler4.

From Coq Require Import List ZArith Arith String Lia.
Import ListNotations.
Open Scope nat_scope.
Open Scope bool_scope.
Open Scope asm.

(**r KV uses allocated lists *)

Fixpoint whole_compiler_aux_v5 (c: code) (fuel pc: nat) (tp_kv: list nat)
  (tp_len: nat) (tp_bin: bin_code): option (list nat * nat * bin_code):=
  match fuel with
  | O => Some (tp_kv, tp_len, tp_bin)
  | S n =>
    if (Nat.eqb (List.length c) pc) then
      Some (tp_kv, tp_len, tp_bin)
    else
    match find_instr pc c with
    | None => None
    | Some bpf_ins =>
      match bpf_ins with
      | Palu32 _ _ _ =>
        match compose_jit_list_v4 c (List.length c - pc) pc tp_len tp_bin with
        | None => None
        | Some (tp_len1, tp_bin1, len) =>
          match ListNat.assign tp_kv pc tp_len with
          | None => None
          | Some kv1 =>
            whole_compiler_aux_v5 c n (pc + len) kv1 tp_len1 tp_bin1
          end
        end

      | Pjmp ofs | Pjmpcmp _ _ _ ofs => (**r check if ins is jump *)
        let lbl := Z.to_nat (Int.signed
          (Int.add Int.one (Int.add (Int.repr (Z.of_nat pc)) ofs))) in
          match find_instr lbl c with
          | None => None
          | Some ins =>
            match ins with
            | Palu32 _ _ _ =>
              match compose_jit_list_v4 c (List.length c - lbl) lbl tp_len tp_bin with
              | None => None
              | Some (tp_len1, tp_bin1, len) =>
                match ListNat.assign tp_kv lbl tp_len with
                | None => None
                | Some kv1 =>
                  whole_compiler_aux_v5 c n (S pc) kv1 tp_len1 tp_bin1
                end
              end
            | _ => whole_compiler_aux_v5 c n (S pc) tp_kv tp_len tp_bin
            end
          end
      | _ => (**r when ins is not jump *)
        whole_compiler_aux_v5 c n (S pc) tp_kv tp_len tp_bin
      end
    end
  end.

Definition whole_compiler_v5 (c: code):
    option (list nat * nat * bin_code):=
  whole_compiler_aux_v5 c (List.length c)
    0%nat (ListNat.create_int_list (List.length c))
    0%nat (List32.create_int_list JITTED_LIST_MAX_LENGTH).


Fixpoint kv_flatten_aux (kv: list (nat * nat)) (l: list nat): option (list nat) :=
  match kv with
  | [] => Some l
  | (k, v) :: tl =>
    match ListNat.assign l k v with
    | None => None
    | Some l' => kv_flatten_aux tl l'
    end
  end.

Definition kv_flatten (kv: list (nat * nat)) (len: nat): option (list nat) :=
  kv_flatten_aux kv (ListNat.create_int_list len).

Lemma ListNat_assign_Some:
  forall l pc v
    (HMAX: pc < List.length l),
      exists l1, ListNat.assign l pc v = Some l1.
Proof.
  induction l; simpl; intros.
  { apply Nat.nlt_0_r in HMAX.
    inversion HMAX.
  }

  destruct pc.
  {
    eexists; reflexivity.
  }

  assert (Heq: pc < Datatypes.length l) by lia.
  eapply IHl in Heq.
  destruct Heq as (l1 & Heq).
  rewrite Heq.
  eexists; reflexivity.
Qed.

Lemma ListNat_assign_same_length:
  forall l pc v l1
    (HA: ListNat.assign l pc v = Some l1),
      List.length l = List.length l1.
Proof.
  induction l; simpl; intros.
  { inversion HA. }

  destruct pc.
  {
    inversion HA; subst.
    simpl.
    reflexivity.
  }

  destruct ListNat.assign as [nl |] eqn: Ha; inversion HA; subst.
  eapply IHl in Ha.
  rewrite Ha.
  simpl; reflexivity.
Qed.

(*
Definition key_value_eq (tp_kv: list (nat * nat)) (l: list nat): Prop :=
  forall k v, List.In (k, v) tp_kv <-> List.nth_error l k = Some v. *)

Lemma ListNat_create_int_list_same_size:
  forall len l,
    ListNat.create_int_list len = l ->
    len = List.length l.
Proof.
  induction len; simpl; intros.
  - inversion H.
    simpl; reflexivity.
  - destruct l.
    + inversion H.
    + injection H as Heq1 Heq2.
      subst n.
      eapply IHlen in Heq2.
      subst len.
      simpl.
      reflexivity.
Qed.

Lemma kv_flatten_aux_same_size:
  forall kv l kv',
    kv_flatten_aux kv l = Some kv' ->
      List.length l = List.length kv'.
Proof.
  induction kv; simpl; intros.
  - inversion H.
    reflexivity.
  - destruct a.
    destruct ListNat.assign eqn: Ha; [| inversion H].
    eapply IHkv in H.
    eapply ListNat_assign_same_length in Ha.
    rewrite Ha; assumption.
Qed.

Lemma kv_flatten_same_size:
  forall kv len kv',
  kv_flatten kv len = Some kv' ->
  len = List.length kv'.
Proof.
  unfold kv_flatten; intros.
  eapply kv_flatten_aux_same_size in H.
  erewrite <- ListNat_create_int_list_same_size in H; eauto.
Qed.

Lemma kv_flatten_aux_concat:
  forall kv l kv' pc tp_len kv1
  (HKVsim : kv_flatten_aux kv l = Some kv')
  (Hkv1_eq : ListNat.assign kv' pc tp_len = Some kv1),
    kv_flatten_aux (kv ++ [(pc, tp_len)]) l = Some kv1.
Proof.
  induction kv; simpl; intros.
  - inversion HKVsim; subst.
    rewrite Hkv1_eq.
    reflexivity.
  - destruct a.
    destruct ListNat.assign eqn: Ha; [| inversion HKVsim].
    eapply IHkv in HKVsim; eauto.
Qed.

Lemma kv_flatten_concat:
  forall kv len kv' pc tp_len kv1
  (HKVsim : kv_flatten kv len = Some kv')
  (Hkv1_eq : ListNat.assign kv' pc tp_len = Some kv1),
    kv_flatten (kv ++ [(pc, tp_len)]) len = Some kv1.
Proof.
  unfold kv_flatten; intros.
  eapply kv_flatten_aux_concat in HKVsim; eauto.
Qed.

Lemma whole_compiler_aux_v5_eq:
  forall fuel c pc tp_kv tp_len tp_bin kv len l tp_kv'
  (Haux: whole_compiler_aux_v4 c fuel pc tp_kv tp_len tp_bin = Some (kv, len, l))
  (HKVsim: kv_flatten tp_kv (List.length c) = Some tp_kv'),
    exists kv',
    kv_flatten kv (List.length c) = Some kv' /\
    whole_compiler_aux_v5 c fuel pc tp_kv' tp_len tp_bin = Some (kv', len, l).
Proof.
  induction fuel; simpl; intros.
  {
    inversion Haux; subst; clear Haux.
    exists tp_kv'.
    split; [assumption |].
    f_equal.
  }

  destruct (_ =? _).
  {
    inversion Haux; subst; clear Haux.
    exists tp_kv'.
    split; [assumption |].
    f_equal.
  }

  destruct find_instr as [ins |] eqn: Hfind; [| inversion Haux].
  destruct ins; try (eapply IHfuel; eauto).
  - destruct compose_jit_list_v4 as
    [((tp_len1 & tp_bin1) & len1)|] eqn: Hcomp; [| inversion Haux].
    assert (Heq: exists kv1, ListNat.assign tp_kv' pc tp_len = Some kv1). {
      unfold find_instr in Hfind.
      destruct nth_error as [ins |] eqn: Hnth; [| inversion Hfind].
      assert (Heq: pc < List.length c).
      - assert (Heq: nth_error c pc <> None).
        + intro HF.
          rewrite Hnth in HF.
          inversion HF.
        + eapply nth_error_Some in Heq; eauto.
      - eapply kv_flatten_same_size in HKVsim.
        rewrite HKVsim in Heq.
        eapply ListNat_assign_Some in Heq.
        apply Heq.
    }
    destruct Heq as (kv1 & Hkv1_eq).
    rewrite Hkv1_eq.
    eapply IHfuel with (tp_kv' := kv1) in Haux; eauto.
    eapply kv_flatten_concat; eauto.
  - destruct find_instr as [ins |] eqn: Hfind1 in Haux; [| inversion Haux].
    rewrite Hfind1.
    destruct ins; try (eapply IHfuel; eauto).
    destruct compose_jit_list_v4 as
    [((tp_len1 & tp_bin1) & len1)|] eqn: Hcomp; [| inversion Haux].
    remember (Z.to_nat (Int.signed (Int.add Int.one (Int.add (Int.repr (Z.of_nat pc)) o)))) as pc1 eqn: Hpc1_eq.
    assert (Heq: exists kv1, ListNat.assign tp_kv' pc1 tp_len = Some kv1). {
      unfold find_instr in Hfind1.
      destruct nth_error as [ins |] eqn: Hnth; [| inversion Hfind1].
      assert (Heq: pc1 < List.length c).
      - assert (Heq: nth_error c pc1 <> None).
        + intro HF.
          rewrite Hnth in HF.
          inversion HF.
        + eapply nth_error_Some in Heq; eauto.
      - eapply kv_flatten_same_size in HKVsim.
        rewrite HKVsim in Heq.
        eapply ListNat_assign_Some in Heq.
        apply Heq.
    }
    destruct Heq as (kv1 & Hkv1_eq).
    rewrite Hkv1_eq.
    eapply IHfuel with (tp_kv' := kv1) in Haux; eauto.
    eapply kv_flatten_concat; eauto.
  - destruct find_instr as [ins |] eqn: Hfind1 in Haux; [| inversion Haux].
    rewrite Hfind1.
    destruct ins; try (eapply IHfuel; eauto).
    destruct compose_jit_list_v4 as
    [((tp_len1 & tp_bin1) & len1)|] eqn: Hcomp; [| inversion Haux].
    remember (Z.to_nat (Int.signed (Int.add Int.one (Int.add (Int.repr (Z.of_nat pc)) o)))) as pc1 eqn: Hpc1_eq.
    assert (Heq: exists kv1, ListNat.assign tp_kv' pc1 tp_len = Some kv1). {
      unfold find_instr in Hfind1.
      destruct nth_error as [ins |] eqn: Hnth; [| inversion Hfind1].
      assert (Heq: pc1 < List.length c).
      - assert (Heq: nth_error c pc1 <> None).
        + intro HF.
          rewrite Hnth in HF.
          inversion HF.
        + eapply nth_error_Some in Heq; eauto.
      - eapply kv_flatten_same_size in HKVsim.
        rewrite HKVsim in Heq.
        eapply ListNat_assign_Some in Heq.
        apply Heq.
    }
    destruct Heq as (kv1 & Hkv1_eq).
    rewrite Hkv1_eq.
    eapply IHfuel with (tp_kv' := kv1) in Haux; eauto.
    eapply kv_flatten_concat; eauto.
Qed.

Lemma whole_compiler_v5_eq:
  forall c kv len l
  (Haux: whole_compiler_v4 c = Some (kv, len, l)),
    exists kv',
    kv_flatten kv (List.length c) = Some kv' /\
    whole_compiler_v5 c = Some (kv', len, l).
Proof.
  unfold whole_compiler_v4, whole_compiler_v5; intros.
  eapply whole_compiler_aux_v5_eq with
    (tp_kv' := (ListNat.create_int_list (Datatypes.length c))) in Haux.
  2:{
    unfold kv_flatten; simpl.
    reflexivity.
  }
  apply Haux.
Qed.