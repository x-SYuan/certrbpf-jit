From compcert.common Require Import Values Memory AST.
From compcert.lib Require Import Integers Maps Zbits.
From compcert.arm Require Import AsmSyntax BinSyntax BinDecode ABinSem.

From bpf.comm Require Import LemmaInt.
From bpf.rbpf32 Require Import TSSyntax JITConfig.

From bpf.jit Require Import Arm32Reg ThumbInsOp ThumbJIT.
From bpf.jit Require Import BitfieldLemma ThumbJITLtac.

From Coq Require Import List ZArith Lia.
Open Scope nat_scope.
Open Scope bool_scope.
Import ListNotations.

Lemma int_le_0_255_size_8:
  forall si,
  Int.cmpu Cle Int.zero si && Int.cmpu Cle si (Int.repr 255) = true ->
  (Int.size si <= 8)%Z.
Proof.
  intros.
  unfold Int.cmpu in *.
  replace 8%Z with (Int.size (Int.repr 255%Z)) by reflexivity.
  unfold Int.size.
  apply Zbits.Zsize_monotone.
  rewrite Bool.andb_true_iff in H.
  destruct H as (Hlo & Hhi).
  apply Cle_Zle_unsigned in Hlo, Hhi.
  replace (Int.unsigned (Int.repr 255)) with 255%Z in * by auto.
  replace (Int.unsigned Int.zero) with 0%Z in Hlo by auto.
  lia.
Qed.


Lemma int_le_0_32_size_5:
  forall si,
  Int.cmpu Cle Int.zero si && Int.cmpu Clt si (Int.repr 32) = true ->
    decode_arm32 si 0 16 = si.
Proof.
  unfold decode_arm32.
  intros.
  unfold Int.cmpu in *.
  assert (Heq: (Int.size si <= 5)%Z).
  - replace 5%Z with (Int.size (Int.repr 31%Z)) by reflexivity.
    unfold Int.size.
    apply Zbits.Zsize_monotone.
    rewrite Bool.andb_true_iff in H.
    destruct H as (Hlo & Hhi).
    apply Cle_Zle_unsigned in Hlo.
    apply Clt_Zlt_unsigned in Hhi.
    replace (Int.unsigned (Int.repr 32)) with 32%Z in * by auto.
    replace (Int.unsigned (Int.repr 31)) with 31%Z in * by auto.
    replace (Int.unsigned Int.zero) with 0%Z in Hlo by auto.
    lia.
  - apply unsigned_bitfield_extract_low_same;
    replace Int.zwordsize with 32%Z by reflexivity; try lia.
Qed.

Lemma decode_add_i:
  forall dst si,
    Int.cmpu Cle Int.zero si && Int.cmpu Cle si (Int.repr 255) = true ->
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) si 8 4)
         (encode_arm32 (int_of_breg dst) ADD_I_OP 0 4) 16 16) =
      Some (Padd (ireg_of_breg dst) (ireg_of_breg dst) (SOimm si)).
Proof.
  intros dst si Hcmp.
  apply int_le_0_255_size_8 in Hcmp as Hle.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  atp_de.
  rewrite int2ireg_int_of_breg_same.
  simpl.

  unfold thumb_constant_range_imm12.
  atp_de.
  atp_de.
  unfold Int.cmpu in Hcmp.
  rewrite Hcmp.
  simpl.
  f_equal.
Qed.

Lemma decode_movw:
  forall si r,
    decode_thumb (mov_int_to_movwt si r MOVW_OP) =
    Some (Pmovw r (Int.unsigned_bitfield_extract 0 16 si)).
Proof.
  unfold decode_thumb, mov_int_to_movwt; intros.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  atp_de.
  simpl.
  rewrite int2ireg_int_of_ireg_same.
  atp_de.
Qed.

Lemma decode_movt:
  forall si r,
    decode_thumb (mov_int_to_movwt (decode_arm32 si 16 16) r MOVT_OP) =
    Some (Pmovt r (Int.unsigned_bitfield_extract 16 16 si)).
Proof.
  unfold decode_thumb, mov_int_to_movwt; intros.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  atp_de.
  simpl.
  rewrite int2ireg_int_of_ireg_same.
  atp_de.
Qed.


Lemma decode_arm32_0_16_16_equal:
  forall si,
  (Int.or (Int.and (decode_arm32 si 0 16) (Int.repr 65535))
                      (Int.shl (Int.unsigned_bitfield_extract 16 16 si) (Int.repr 16))) = si.
Proof.
  intros.
  assert (Heq: (Int.or (Int.and (decode_arm32 si 0 16) (Int.repr 65535))
                      (Int.shl (Int.unsigned_bitfield_extract 16 16 si) (Int.repr 16))) =
    Int.bitfield_insert 16 16 (decode_arm32 si 0 16) (Int.unsigned_bitfield_extract 16 16 si)). {
    unfold Int.bitfield_insert.
    rewrite Int.or_commut.
    assert (Heq: (Int.unsigned_bitfield_extract 16 16 si) =
      (Int.zero_ext 16 (Int.unsigned_bitfield_extract 16 16 si))). {
      unfold Int.unsigned_bitfield_extract.
      rewrite Int.zero_ext_idem. f_equal. lia.
    }
    rewrite <- Heq; clear Heq.
    replace (Int.not (Int.shl (Int.repr (two_p 16 - 1)) (Int.repr 16))) with (Int.repr 65535) by reflexivity.
    f_equal.
  }
  rewrite Heq; clear Heq.
  assert (Heq: Int.bitfield_insert 16 16 (decode_arm32 si 0 16)
    (Int.unsigned_bitfield_extract 16 16 si) = si). {
    unfold decode_arm32.
    erewrite unsigned_bitfield_extract_extend;
      replace Int.zwordsize with 32%Z by reflexivity; try lia.
    simpl.
    unfold Int.unsigned_bitfield_extract.
    fold Int.zero; rewrite Int.shru_zero.
    rewrite Int.zero_ext_above.
    reflexivity.
    replace Int.zwordsize with 32%Z by reflexivity; try lia.
  }
  rewrite Heq; clear Heq.
  reflexivity.
Qed.

Lemma decode_sub_i:
  forall dst si,
    Int.cmpu Cle Int.zero si && Int.cmpu Cle si (Int.repr 255) = true ->
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) si 8 4) (encode_arm32 (int_of_breg dst) SUB_I_OP 0 4) 16 16) =
      Some (Psub (ireg_of_breg dst) (ireg_of_breg dst)
                (SOimm si)).
Proof.
  intros dst si Hcmp.
  apply int_le_0_255_size_8 in Hcmp as Hle.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  atp_de.
  simpl.

  rewrite int2ireg_int_of_breg_same.

  unfold thumb_constant_range_imm12.
  atp_de.
  atp_de.
  unfold Int.cmpu in Hcmp.
  rewrite Hcmp.

  simpl.
  f_equal.
Qed.

Lemma decode_orr_i:
  forall dst si,
    Int.cmpu Cle Int.zero si && Int.cmpu Cle si (Int.repr 255) = true ->
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) si 8 4) (encode_arm32 (int_of_breg dst) ORR_I_OP 0 4) 16 16) =
      Some (Porr (ireg_of_breg dst) (ireg_of_breg dst)
                (SOimm si)).
Proof.
  intros dst si Hcmp.
  apply int_le_0_255_size_8 in Hcmp as Hle.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  atp_de.
  simpl.

  rewrite int2ireg_int_of_breg_same.

  unfold thumb_constant_range_imm12.
  atp_de.
  atp_de.
  unfold Int.cmpu in Hcmp.
  rewrite Hcmp.

  simpl.
  f_equal.
Qed.

Lemma decode_rsb_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) Int.zero 8 4) (encode_arm32 (int_of_ireg src) RSB_I_OP 0 4)
         16 16) = Some (Prsb (ireg_of_breg dst) src (SOimm Int.zero)).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  simpl.
  rewrite int2ireg_int_of_breg_same.

  unfold thumb_constant_range_imm12.
  atp_de.
  assert (Heq: (Int.unsigned_bitfield_extract 0 8 Int.zero) = Int.zero) by atp_de; rewrite Heq; clear Heq.
  simpl.
  replace (negb (Int.ltu Int.zero Int.zero) && negb (Int.ltu (Int.repr 255) Int.zero)) with true by auto.
  rewrite int2ireg_int_of_ireg_same.
  f_equal.
Qed.

Lemma decode_xor_i:
  forall dst si,
    Int.cmpu Cle Int.zero si && Int.cmpu Cle si (Int.repr 255) = true ->
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) si 8 4) (encode_arm32 (int_of_breg dst) EOR_I_OP 0 4) 16 16) =
      Some (Peor (ireg_of_breg dst) (ireg_of_breg dst) (SOimm si)).
Proof.
  intros dst si Hcmp.
  apply int_le_0_255_size_8 in Hcmp as Hle.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  atp_de.
  simpl.

  rewrite int2ireg_int_of_breg_same.

  unfold thumb_constant_range_imm12.
  atp_de.
  atp_de.
  unfold Int.cmpu in Hcmp.
  rewrite Hcmp.

  simpl.
  f_equal.
Qed.

Lemma decode_add_i_11:
  forall si,
  Int.cmpu Cle Int.zero si && Int.cmpu Cle si (Int.repr 255) = true ->
    decode_thumb
      (encode_arm32 (encode_arm32 (Int.repr 11) si 8 4)
         (encode_arm32 (Int.repr 11) ADD_I_OP 0 4) 16 16) = 
    Some (Padd IR11 IR11 (SOimm si)).
Proof.
  intros si Hcmp.
  apply int_le_0_255_size_8 in Hcmp as Hle.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  atp_de.
  simpl.

  assert (Heq: int2ireg (Int.repr 11) = Some IR11) by atp_de; rewrite Heq; clear Heq.

  unfold thumb_constant_range_imm12.
  atp_de.
  atp_de.
  unfold Int.cmpu in Hcmp.
  rewrite Hcmp.

  simpl.
  f_equal.
Qed.