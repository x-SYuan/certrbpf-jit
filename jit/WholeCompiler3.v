From compcert.lib Require Import Integers Coqlib.
From compcert.common Require Import AST Values Memory.
From compcert.arm Require Import ABinSem BinDecode.

From bpf.comm Require Import JITCall.
From bpf.rbpf32 Require Import JITConfig TSSyntax TSDecode Analyzer.
From bpf.jit Require Import ThumbJIT WholeCompiler WholeCompiler0 WholeCompiler1.
From bpf.jit Require Import WholeCompiler2 ThumbJIT1 ListSetRefinement ThumbJIT1proof ListSetSort.

From Coq Require Import List ZArith Arith String Lia.
Import ListNotations.
Open Scope nat_scope.
Open Scope bool_scope.
Open Scope asm.

(**r use refined listset *)

Fixpoint get_alu32_jit_list_v3 (c: code) (fuel pc counter: nat) (ld_set st_set: listset_regmap):
  option (bin_code * listset_regmap * listset_regmap * nat) :=
  match fuel with
  | O => None
  | S n =>
      match List.nth_error c pc with
      | None => None
      | Some ins =>
        match decode_ind ins with
        | None => None
        | Some bpf_ins =>
          match bpf_ins with
          | Palu32 op dst src =>
            match jit_one_v1 op dst src ld_set st_set with
            | None => None
            | Some (l1, ld1, st1) =>
              match get_alu32_jit_list_v3 c n (S pc) (S counter) ld1 st1 with
              | None => None
              | Some (l2, ld2, st2, counter2) => Some (l1 ++ l2, ld2, st2, counter2)
              end
            end
          | _ => Some ([], ld_set, st_set, counter)
          end
        end
      end
  end.

Definition compose_jit_list_aux_v3 (c: code) (fuel pc counter: nat) (ld_set st_set: listset_regmap): option (bin_code * nat) :=
  match get_alu32_jit_list_v3 c fuel pc counter ld_set st_set with
  | None => None
  | Some (bl, ld_set, st_set, n) =>
    match jit_alu32_thumb_pc n with
    | None => None
    | Some l_pc => Some (jit_alu32_pre ++ bl ++ l_pc ++
      (jit_alu32_thumb_store_v1 st_set) ++
      (jit_alu32_thumb_reset_v1 ld_set) ++ jit_alu32_post, n)
    end
  end.

Definition compose_jit_list_v3 (c: code) (fuel pc: nat): option (bin_code * nat) :=
  compose_jit_list_aux_v3 c fuel pc 0%nat init_listset_regmap init_listset_regmap.

Fixpoint whole_compiler_aux_v3 (c: code) (fuel pc base: nat):
    option (list (nat * nat) * bin_code) :=
  match fuel with
  | O => Some ([], [])
  | S n =>
    if (Nat.eqb (List.length c) pc) then
      Some ([], [])
    else
    match find_instr pc c with
    | None => None
    | Some bpf_ins =>
      match bpf_ins with
      | Palu32 _ _ _ =>
        match compose_jit_list_v3 c (List.length c - pc) pc with
        | None => None
        | Some (bl, len) =>
          if Nat.ltb (base + List.length bl) JITTED_LIST_MAX_LENGTH then
            match whole_compiler_aux_v3 c n (pc + len) (base + (List.length bl)) with
            | None => None
            | Some (kv, l) => Some ((pc, base) :: kv, bl ++ l) (*
              if (List.existsb (fun x => Nat.eqb (fst x) pc) kv) then
                Some (kv, l)
              else
                Some ((pc, base) :: kv, bl ++ l) *)
            end
          else
            None
        end

      | Pjmp ofs | Pjmpcmp _ _ _ ofs => (**r check if ins is jump *)
        let lbl := Z.to_nat (Int.signed
          (Int.add Int.one (Int.add (Int.repr (Z.of_nat pc)) ofs))) in
          match find_instr lbl c with
          | None => None
          | Some ins =>
            match ins with
            | Palu32 _ _ _ =>
              match compose_jit_list_v3 c (List.length c - lbl) lbl with
              | None => None
              | Some (bl, _) =>
                if Nat.ltb (base + List.length bl) JITTED_LIST_MAX_LENGTH then
                  match whole_compiler_aux_v3 c n (S pc) (base + (List.length bl)) with
                  | None => None
                  | Some (kv, l) => Some ((lbl, base) :: kv, bl ++ l) (*
                    if (List.existsb (fun x => Nat.eqb (fst x) lbl) kv) then
                      Some (kv, l)
                    else
                      Some ((lbl, base) :: kv, bl ++ l) *)
                  end
                else
                  None
              end
            | _ => whole_compiler_aux_v3 c n (S pc) base
            end
          end
      | _ => (**r when ins is not jump *)
        whole_compiler_aux_v3 c n (S pc) base
      end
    end
  end.

Definition whole_compiler_v3 (c: code):
    option (list (nat * nat) * bin_code) :=
  whole_compiler_aux_v3 c (List.length c) 0%nat 0%nat.

Lemma get_alu32_jit_list_v3_eq:
  forall fuel pc c counter ld_set1 lr_ld1 st_set1 lr_st1 bl ld_set2 st_set2 n
  (Hnodup1: NoDup ld_set1)
  (Hnodup2: NoDup st_set1)
  (Hst1: eq_set_map ld_set1 lr_ld1)
  (Hst2: eq_set_map st_set1 lr_st1)
  (Hjit: get_alu32_jit_list c fuel pc counter ld_set1 st_set1 = Some (bl, ld_set2, st_set2, n)),
    exists lr_ld2 lr_st2,
      get_alu32_jit_list_v3 c fuel pc counter lr_ld1 lr_st1 = Some (bl, lr_ld2, lr_st2, n) /\
    eq_set_map ld_set2 lr_ld2 /\
    eq_set_map st_set2 lr_st2 /\
    NoDup ld_set2 /\
    NoDup st_set2.
Proof.
  induction fuel; intros.
  { simpl in Hjit.
    inversion Hjit.
  }

  simpl in Hjit.
  simpl.
  destruct nth_error as [ins| ] eqn: Hnth; [| inversion Hjit].
  destruct decode_ind as [bpf_ins |] eqn: Hdecode; [| inversion Hjit].
  destruct bpf_ins; inversion Hjit; subst; try (
    eexists; eexists; split; [reflexivity | repeat (split; auto)]).
  clear H0.
  destruct jit_one as [((l1 & ld1) & st1) |] eqn: Hone; [| inversion Hjit].
  destruct get_alu32_jit_list as [(((l2 & ld2) & st2) & n2) |] eqn: Hget; [| inversion Hjit].
  eapply jit_one_v1_eq in Hone; eauto.
  destruct Hone as (lr_ld2 & lr_st2 & Hone & Heq1 & Heq2 & Hno1 & Hno2).
  rewrite Hone.
  eapply IHfuel in Hget; eauto.
  destruct Hget as (lr_ld3 & lr_st3 & Hget & Heq3 & Heq4 & Hno3 & Hno4).
  rewrite Hget.
  inversion Hjit; subst.
  eexists; eexists; split; [reflexivity |].
  repeat (split; auto).
Qed.

Lemma compose_jit_list_v3_eq:
  forall fuel pc c bl n,
    compose_jit_list c fuel pc = Some (bl, n) ->
      compose_jit_list_v3 c fuel pc = Some (bl, n).
Proof.
  unfold compose_jit_list_v3, compose_jit_list,
    compose_jit_list_aux, compose_jit_list_aux_v3; intros.
  destruct get_alu32_jit_list as [(((tl & ld_set) & st_set) & k) |] eqn: Hget; [| inversion H].
  assert (Heq_nil: eq_set_map [] init_listset_regmap). {
    unfold eq_set_map, init_listset_regmap.
    intros.
    split; intro HF.
    - inversion HF.
    - destruct r; simpl in HF; inversion HF.
  }
  eapply get_alu32_jit_list_v3_eq in Hget; eauto; try (apply NoDup_nil).
  destruct Hget as (lr_ld2 & lr_st2 & Hget & Heq1 & Heq2 & Hno1 & Hno2).
  rewrite Hget.
  destruct jit_alu32_thumb_pc as [l_pc |]; [| inversion H].
  rewrite <- H.
  apply eq_set_map_sort_list_same in Heq1.
  apply eq_set_map_sort_list_same in Heq2.
  repeat f_equal.
  - erewrite jit_alu32_thumb_store_v1_eq with (st_set := sort_listset st_set); eauto.
    + apply NoDup_sort_listset; auto.
    + apply sort_sort_listset.
  - erewrite jit_alu32_thumb_reset_v1_eq with (ld_set := sort_listset ld_set); eauto.
    + apply NoDup_sort_listset; auto.
    + apply sort_sort_listset.
Qed.

Lemma whole_compiler_aux_v3_same:
  forall fuel c pc base kv l,
    whole_compiler_aux_v2 c fuel pc base = Some (kv, l) ->
    whole_compiler_aux_v3 c fuel pc base = Some (kv, l).
Proof.
  induction fuel; simpl; intros c pc base kv l Haux.
  { auto. }

  destruct (_ =? _) eqn: Heq_len.
  { auto. }

  destruct find_instr as [ins|] eqn: Hfind; [| inversion Haux].
  destruct ins; try (eapply IHfuel; eauto; lia).
  - destruct compose_jit_list as [(l1 & len )|] eqn: HL1; [| inversion Haux].
    erewrite compose_jit_list_v3_eq; eauto.
    destruct (_ <? _) eqn: HMAX; [| inversion Haux].

    destruct whole_compiler_aux_v2 as [(kvk, lk)|] eqn: Hauxk in Haux;[| inversion Haux].
    erewrite IHfuel; eauto.

  - destruct find_instr as [ins_pc|] eqn: Hfind1 in Haux; [| inversion Haux].
    rewrite Hfind1.
    destruct ins_pc; try (eapply IHfuel; eauto; lia).

    destruct compose_jit_list as [(l1 & len )|] eqn: HL1; [| inversion Haux].
    erewrite compose_jit_list_v3_eq; eauto.
    destruct (_ <? _) eqn: HMAX; [| inversion Haux].

    destruct whole_compiler_aux_v2 as [(kvk, lk)|] eqn: Hauxk in Haux;[| inversion Haux].
    erewrite IHfuel; eauto.

  - destruct find_instr as [ins_pc|] eqn: Hfind1 in Haux; [| inversion Haux].
    rewrite Hfind1.
    destruct ins_pc; try (eapply IHfuel; eauto; lia).

    destruct compose_jit_list as [(l1 & len )|] eqn: HL1; [| inversion Haux].
    erewrite compose_jit_list_v3_eq; eauto.
    destruct (_ <? _) eqn: HMAX; [| inversion Haux].

    destruct whole_compiler_aux_v2 as [(kvk, lk)|] eqn: Hauxk in Haux;[| inversion Haux].
    erewrite IHfuel; eauto.
Qed.

Theorem whole_compiler_v3_same:
  forall c kv l,
    whole_compiler_v2 c = Some (kv, l) ->
    whole_compiler_v3 c = Some (kv, l).
Proof.
  unfold whole_compiler_v2, whole_compiler_v3; intros.
  eapply whole_compiler_aux_v3_same; eauto.
Qed.