From compcert.lib Require Import Integers Coqlib.
From compcert.common Require Import AST Values Memory.
From compcert.arm Require Import ABinSem BinDecode.

From bpf.comm Require Import ListAsArray LemmaNat JITCall.
From bpf.rbpf32 Require Import JITConfig TSSyntax TSDecode Analyzer.
From bpf.jit Require Import ThumbJIT WholeCompiler.
From bpf.jit Require Import ThumbJIT1 ListSetRefinement ThumbJIT1proof ListSetSort.
From bpf.jit Require Import ThumbJIT2 WholeCompiler3.

From Coq Require Import List ZArith Arith String Lia.
Import ListNotations.
Open Scope nat_scope.
Open Scope bool_scope.
Open Scope asm.

(**r use refined listset *)

Fixpoint get_alu32_jit_list_v4 (c: code) (fuel pc counter: nat) (ld_set st_set: listset_regmap)
  (tp_len: nat) (tp_bin: bin_code): option (nat * bin_code * listset_regmap * listset_regmap * nat):=
  match fuel with
  | O => None
  | S n =>
      match List.nth_error c pc with
      | None => None
      | Some ins =>
        match decode_ind ins with
        | None => None
        | Some bpf_ins =>
          match bpf_ins with
          | Palu32 op dst src =>
            match jit_one_v2 op dst src ld_set st_set tp_len tp_bin with
            | None => None
            | Some (tp_len1, tp_bin1, ld1, st1) =>
              get_alu32_jit_list_v4 c n (S pc) (S counter) ld1 st1 tp_len1 tp_bin1
            end
          | _ => Some (tp_len, tp_bin, ld_set, st_set, counter)
          end
        end
      end
  end.

Definition compose_jit_list_aux_v4 (c: code) (fuel pc counter: nat) (ld_set st_set: listset_regmap)
  (tp_len: nat) (tp_bin: bin_code): option (nat * bin_code * nat):=
  match jit_alu32_pre_v2 tp_len tp_bin with
  | None => None
  | Some (tp_len1, tp_bin1) =>
    match get_alu32_jit_list_v4 c fuel pc counter ld_set st_set tp_len1 tp_bin1 with
    | None => None
    | Some (tp_len2, tp_bin2, ld_set2, st_set2, n) =>
      match jit_alu32_thumb_pc_v2 n tp_len2 tp_bin2 with
      | None => None
      | Some (tp_len3, tp_bin3) =>
        match jit_alu32_thumb_store_v2 st_set2 tp_len3 tp_bin3 with
        | None => None
        | Some (tp_len4, tp_bin4) =>
          match jit_alu32_thumb_reset_v2 ld_set2 tp_len4 tp_bin4 with
          | None => None
          | Some (tp_len5, tp_bin5) =>
            match jit_alu32_post_v2 tp_len5 tp_bin5 with
            | None => None
            | Some (tp_len6, tp_bin6) => Some (tp_len6, tp_bin6, n)
            end
          end
        end
      end
    end
  end.

Definition compose_jit_list_v4 (c: code) (fuel pc: nat)
  (tp_len: nat) (tp_bin: bin_code): option (nat * bin_code * nat):=
  compose_jit_list_aux_v4 c fuel pc 0%nat init_listset_regmap init_listset_regmap tp_len tp_bin.

Fixpoint whole_compiler_aux_v4 (c: code) (fuel pc: nat) (tp_kv: list (nat * nat))
  (tp_len: nat) (tp_bin: bin_code): option (list (nat * nat) * nat * bin_code):=
  match fuel with
  | O => Some (tp_kv, tp_len, tp_bin)
  | S n =>
    if (Nat.eqb (List.length c) pc) then
      Some (tp_kv, tp_len, tp_bin)
    else
    match find_instr pc c with
    | None => None
    | Some bpf_ins =>
      match bpf_ins with
      | Palu32 _ _ _ =>
        match compose_jit_list_v4 c (List.length c - pc) pc tp_len tp_bin with
        | None => None
        | Some (tp_len1, tp_bin1, len) =>
          whole_compiler_aux_v4 c n (pc + len) (tp_kv ++ [(pc, tp_len)]) tp_len1 tp_bin1
        end

      | Pjmp ofs | Pjmpcmp _ _ _ ofs => (**r check if ins is jump *)
        let lbl := Z.to_nat (Int.signed
          (Int.add Int.one (Int.add (Int.repr (Z.of_nat pc)) ofs))) in
          match find_instr lbl c with
          | None => None
          | Some ins =>
            match ins with
            | Palu32 _ _ _ =>
              match compose_jit_list_v4 c (List.length c - lbl) lbl tp_len tp_bin with
              | None => None
              | Some (tp_len1, tp_bin1, len) =>
                whole_compiler_aux_v4 c n (S pc) (tp_kv ++ [(lbl, tp_len)]) tp_len1 tp_bin1
              end
            | _ => whole_compiler_aux_v4 c n (S pc) tp_kv tp_len tp_bin
            end
          end
      | _ => (**r when ins is not jump *)
        whole_compiler_aux_v4 c n (S pc) tp_kv tp_len tp_bin
      end
    end
  end.

Definition whole_compiler_v4 (c: code):
    option (list (nat * nat) * nat * bin_code):=
  whole_compiler_aux_v4 c (List.length c) 0%nat [] 0%nat (List32.create_int_list JITTED_LIST_MAX_LENGTH).


Fixpoint list_in_bin_code (l: bin_code) (tp_len: nat) (tp_bin: bin_code): bool :=
  match l with
  | [] => true
  | hd :: tl =>
    match List.nth_error tp_bin tp_len with
    | None => false
    | Some hd1 =>
      if Int.eq hd1 hd then
        list_in_bin_code tl (S tp_len) tp_bin
      else
        false
    end
  end.

Lemma list_in_bin_code_concat_iff:
  forall l1 l2 tp_len tp_bin,
    list_in_bin_code (l1 ++ l2) tp_len tp_bin = true <->
    list_in_bin_code l1 tp_len tp_bin = true /\
    list_in_bin_code l2 (tp_len + List.length l1) tp_bin = true.
Proof.
  induction l1; simpl; intros.
  { split; intro HF.
    - split;[reflexivity | ].
      rewrite Nat.add_0_r.
      assumption.
    - destruct HF as (_ & HF).
      rewrite Nat.add_0_r in HF.
      assumption.
  }

  split; intro Hsubset.
  - destruct nth_error as [hd1 |] eqn: Hnth; [| inversion Hsubset].
    destruct Int.eq eqn: Heq; [| inversion Hsubset].
    eapply Int.same_if_eq in Heq.
    subst a.
    eapply IHl1 in Hsubset; eauto.
    assert (Heq: S tp_len + Datatypes.length l1 = tp_len + S (Datatypes.length l1)) by lia.
    rewrite <- Heq.
    assumption.
  - destruct Hsubset as (Hsubset & Hlist).
    destruct nth_error as [hd1 |] eqn: Hnth; [| inversion Hsubset].
    destruct Int.eq eqn: Heq; [| inversion Hsubset].
    eapply Int.same_if_eq in Heq.
    subst a.
    eapply IHl1; eauto.
    split.
    + assumption.
    + assert (Heq: S tp_len + Datatypes.length l1 = tp_len + S (Datatypes.length l1)) by lia.
      rewrite Heq.
      assumption.
Qed.

Lemma List32_assign_list_in_bin_code_other:
  forall l1 len1 tp a len2 tp3
    (HA: len1 + List.length l1 <= len2 \/ len2 + 1 <= len1)
    (Heq1 : list_in_bin_code l1 len1 tp = true)
    (Heq2 : List32.assign' tp len2 a = Some tp3),
      list_in_bin_code l1 len1 tp3 = true.
Proof.
  induction l1; simpl; intros.
  {
    reflexivity.
  }

  destruct nth_error as [hd1 |] eqn: Hnth; [| inversion Heq1].
  destruct Int.eq eqn: Heq; [| inversion Heq1].
  apply Int.same_if_eq in Heq.
  subst hd1.
  eapply List32_assign_nth_error_other with (ofs' := len1) in Heq2 as Heq.
  2:{
    lia.
  }
  rewrite <- Heq.
  rewrite Hnth.
  rewrite Int.eq_true.
  specialize (IHl1 (S len1) tp a0 len2 tp3). clear Heq.
  assert (Heq: S len1 + Datatypes.length l1 <= len2 \/ len2 + 1 <= S len1) by lia.
  specialize (IHl1 Heq Heq1 Heq2); clear Heq.
  assumption.
Qed.

Lemma jit_call_save_add_v2_eq:
  forall a l l1 len tp
    (HMAX: len + List.length l1 < List.length tp)
    (HJIT: jit_call_save_add_v1 a l = l1),
      exists tp1,
      jit_call_save_add_v2 a l len tp = Some (len + List.length l1, tp1) /\
      list_in_bin_code l1 len tp1 = true /\
      List.length tp = List.length tp1 /\
      (forall i, i < len -> List.nth_error tp i = List.nth_error tp1 i) /\
      (forall ln len_n, list_in_bin_code ln len_n tp = true ->
        len_n + List.length ln <= len ->
        list_in_bin_code ln len_n tp1 = true).
Proof.
  unfold jit_call_save_add_v1, jit_call_save_add_v2; intros.
  destruct eval_listset_regmap eqn: Heval.
  { subst l1.
    simpl.
    rewrite Nat.add_0_r.
    eexists.
    split; [reflexivity |].
    split; [reflexivity |].
    split; [reflexivity |].
    split.
    - intros i Hle.
      reflexivity.
    - intros ln len_n Hln_eq Hle.
      assumption.
  }

  destruct (_ && _) eqn: Hand.
  {
    subst l1.
    simpl in HMAX.
    unfold tp_bin_add.
    assert (Heq0: len < Datatypes.length tp) by lia.
    eapply List32_assign_Some in Heq0.
    destruct Heq0 as (l0 & Hl0_eq).
    rewrite Hl0_eq.

    apply List32_assign_same_length in Hl0_eq as Heq.
    assert (Heq1: S len < Datatypes.length l0) by lia.
    eapply List32_assign_Some in Heq1.
    destruct Heq1 as (l1 & Hl1_eq).
    rewrite Hl1_eq.
    eexists; split; [simpl; reflexivity |].
    simpl.
    eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
    eapply List32_assign_nth_error_other with (ofs' := len) in Hl1_eq as Heq2; eauto.
    rewrite <- Heq2.
    rewrite Heq1.
    rewrite Int.eq_true.
    clear Heq Heq1 Heq2.
    eapply List32_assign_nth_error_same in Hl1_eq as Heq.
    destruct l1.
    - simpl in Heq.
      inversion Heq.
    - simpl in Heq.
      rewrite Heq.
      rewrite Int.eq_true.
      split; [reflexivity| ].
      eapply List32_assign_same_length in Hl0_eq as Heq0.
      eapply List32_assign_same_length in Hl1_eq as Heq1.
      rewrite Heq0.
      split; [ assumption |].
      split.
      + intros i0 Hle.
        eapply List32_assign_nth_error_other with (ofs' := i0) in Hl1_eq;
          eauto; [| lia].
        rewrite <- Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := i0) in Hl0_eq;
          eauto.
        lia.
      + intros ln len_n Hln_eq Hle.
        eapply List32_assign_list_in_bin_code_other
          with (len1 := len_n) in Hl1_eq; eauto.
        eapply List32_assign_list_in_bin_code_other
          with (len1 := len_n) in Hl0_eq; eauto.
  }

  subst l1.
  simpl in HMAX.
  unfold tp_bin_add.
  assert (Heq0: len < Datatypes.length tp) by lia.
  eapply List32_assign_Some in Heq0.
  destruct Heq0 as (l0 & Hl0_eq).
  rewrite Hl0_eq.
  exists l0; split; [simpl; reflexivity |].
  simpl.
  eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
  rewrite Heq1.
  rewrite Int.eq_true.
  split; [reflexivity |].
  eapply List32_assign_same_length in Hl0_eq as Heq0.
  split; [ assumption |].
  split.
  - intros i0 Hle.
    eapply List32_assign_nth_error_other with (ofs' := i0) in Hl0_eq;
      eauto.
    lia.
  - intros ln len_n Hln_eq Hle.
    eapply List32_assign_list_in_bin_code_other
      with (len1 := len_n) in Hl0_eq; eauto.
Qed.

Lemma jit_call_save_add_v2_same_tp:
  forall a ld_set tp_len tp_bin tp1
  (HJIT: jit_call_save_add_v2 a ld_set tp_len tp_bin = Some (tp_len, tp1)),
    tp_bin  = tp1.
Proof.
  unfold jit_call_save_add_v2; intros.
  destruct eval_listset_regmap.
  { inversion HJIT; subst.
    reflexivity.
  }

  destruct (_ && _).
  {
    destruct tp_bin_add.
    - destruct tp_bin_add; inversion HJIT.
      exfalso.
      eapply nat_add_n_neq in H0; eauto.
    - inversion HJIT.
  }

  destruct tp_bin_add.
  - inversion HJIT.
    exfalso.
    eapply nat_add_n_neq in H0; eauto.
  - inversion HJIT.
Qed.

Lemma jit_call_save_reg_v2_eq:
  forall l tp_len tp_bin a b ld_set st_set ld_set1 st_set1
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HJIT : jit_call_save_reg_v1 a b ld_set st_set = (l, ld_set1, st_set1)),
      exists tp_bin1,
      jit_call_save_reg_v2 a b ld_set st_set tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1, ld_set1, st_set1) /\
      list_in_bin_code l tp_len tp_bin1 = true /\
      List.length tp_bin = List.length tp_bin1 /\
      (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
      (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
        len_n + List.length ln <= tp_len ->
        list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_call_save_reg_v1, jit_call_save_reg_v2.
  simpl; intros.

  injection HJIT as Hl_eq Hld_eq Hst_eq.
  remember (jit_call_save_add_v1 a ld_set) as l1_eq eqn: Hl1_eq.
  remember (jit_call_save_add_v1 b (listset_add_regmap a ld_set)) as l2_eq eqn: Hl2_eq.
  symmetry in Hl1_eq, Hl2_eq.

  eapply jit_call_save_add_v2_eq with (len := tp_len) (tp := tp_bin) in Hl1_eq; eauto.
  2:{
    assert (Heq: List.length l1_eq <= List.length l).
    rewrite <- Hl_eq.
    rewrite app_length.
    lia.
    simpl in Heq.
    lia.
  }
  destruct Hl1_eq as (tp1 & Heq1 & Heq2 & Hlen1_eq & Hnth1_eq & Hlist1_eq).
  rewrite Heq1.

  eapply jit_call_save_add_v2_eq with (len := (tp_len + Datatypes.length l1_eq))
    (tp := tp1) in Hl2_eq; eauto.
  2:{
    rewrite <- Nat.add_assoc.
    rewrite <- app_length.
    rewrite Hl_eq.
    simpl.
    lia.
  }
  destruct Hl2_eq as (tp2 & Heq3 & Heq4 & Hlen2_eq & Hnth2_eq & Hlist2_eq).
  rewrite Heq3.
  subst l.
  rewrite app_length in HMAX.
  exists tp2.
  split.
  - subst ld_set1 st_set1.
    rewrite <- Nat.add_assoc.
    rewrite app_length.
    f_equal.
  - split.
    + eapply list_in_bin_code_concat_iff; eauto.
    + split.
      * rewrite Hlen1_eq.
        assumption.
      * split.
        {
          intros i Hle.
          erewrite Hnth1_eq; eauto.
          erewrite Hnth2_eq; eauto.
          lia.
        }
        {
          intros ln len_n Hlist_eq Hle.
          eapply Hlist2_eq; eauto.
          lia.
        }
Qed.

Lemma bpf_alu32_to_thumb_reg_v2_eq:
  forall l a b c tp_len tp_bin
    (HMAX : tp_len + Datatypes.length l < Datatypes.length tp_bin)
    (Hthumb : bpf_alu32_to_thumb_reg a b c = Some l),
      exists tp_bin1 : bin_code,
        bpf_alu32_to_thumb_reg_v2 a b c tp_len tp_bin =
          Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold bpf_alu32_to_thumb_reg, bpf_alu32_to_thumb_reg_v2; intros.
  destruct a.
  - injection Hthumb as Hl_eq.
    subst l; simpl in *.
    unfold tp_bin_add.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l1 & Heq).
    rewrite Heq.
    exists l1.
    split.
    + f_equal.
    + erewrite List32_assign_nth_error_same; eauto.
      rewrite Int.eq_true.
      split; [reflexivity| ].
      split.
      * eapply List32_assign_same_length; eauto.
      * split.
        { intros i Hlen.
          eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
          lia.
        }
        {
          intros ln len_n Hl_eq Hle.
          eapply List32_assign_list_in_bin_code_other in Heq; eauto.
        }
  - injection Hthumb as Hl_eq.
    subst l; simpl in *.
    unfold tp_bin_add.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l1 & Heq).
    rewrite Heq.
    exists l1.
    split.
    + f_equal.
    + erewrite List32_assign_nth_error_same; eauto.
      rewrite Int.eq_true.
      split; [reflexivity| ].
      split.
      * eapply List32_assign_same_length; eauto.
      * split.
        {
          intros i Hlen.
          eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
          lia.
        }
        {
          intros ln len_n Hl_eq Hle.
          eapply List32_assign_list_in_bin_code_other in Heq; eauto.
        }
  - injection Hthumb as Hl_eq.
    subst l; simpl in *.
    unfold tp_bin_add.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l1 & Heq).
    rewrite Heq.
    exists l1.
    split.
    + f_equal.
    + erewrite List32_assign_nth_error_same; eauto.
      rewrite Int.eq_true.
      split; [reflexivity| ].
      split.
      * eapply List32_assign_same_length; eauto.
      * split.
        {
          intros i Hlen.
          eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
          lia.
        }
        {
          intros ln len_n Hl_eq Hle.
          eapply List32_assign_list_in_bin_code_other in Heq; eauto.
        }
  - inversion Hthumb.
  - injection Hthumb as Hl_eq.
    subst l; simpl in *.
    unfold tp_bin_add.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l1 & Heq).
    rewrite Heq.
    exists l1.
    split.
    + f_equal.
    + erewrite List32_assign_nth_error_same; eauto.
      rewrite Int.eq_true.
      split; [reflexivity| ].
      split.
      * eapply List32_assign_same_length; eauto.
      * split.
        {
          intros i Hlen.
          eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
          lia.
        }
        {
          intros ln len_n Hl_eq Hle.
          eapply List32_assign_list_in_bin_code_other in Heq; eauto.
        }
  - injection Hthumb as Hl_eq.
    subst l; simpl in *.
    unfold tp_bin_add.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l1 & Heq).
    rewrite Heq.
    exists l1.
    split.
    + f_equal.
    + erewrite List32_assign_nth_error_same; eauto.
      rewrite Int.eq_true.
      split; [reflexivity| ].
      split.
      * eapply List32_assign_same_length; eauto.
      * split.
        {
          intros i Hlen.
          eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
          lia.
        }
        {
          intros ln len_n Hl_eq Hle.
          eapply List32_assign_list_in_bin_code_other in Heq; eauto.
        }
  - inversion Hthumb.
  - inversion Hthumb.
  - inversion Hthumb.
  - inversion Hthumb.
  - injection Hthumb as Hl_eq.
    subst l; simpl in *.
    unfold tp_bin_add.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l1 & Heq).
    rewrite Heq.
    exists l1.
    split.
    + f_equal.
    + erewrite List32_assign_nth_error_same; eauto.
      rewrite Int.eq_true.
      split; [reflexivity| ].
      split.
      * eapply List32_assign_same_length; eauto.
      * split.
        {
          intros i Hlen.
          eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
          lia.
        }
        {
          intros ln len_n Hl_eq Hle.
          eapply List32_assign_list_in_bin_code_other in Heq; eauto.
        }
  - destruct Arm32Reg.reg_ireg_eqb.
    { inversion Hthumb; subst; clear Hthumb.
      exists tp_bin.
      simpl.
      rewrite Nat.add_0_r.
      do 3 (split; [reflexivity| ]).
      split.
      - intros i Hle.
        reflexivity.
      - intros ln len_n Hl_eq Hle.
        assumption.
    }

    injection Hthumb as Hl_eq.
    subst l; simpl in *.
    unfold tp_bin_add.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l1 & Heq).
    rewrite Heq.
    exists l1.
    split.
    + f_equal.
    + erewrite List32_assign_nth_error_same; eauto.
      rewrite Int.eq_true.
      split; [reflexivity| ].
      split.
      * eapply List32_assign_same_length; eauto.
      * split.
        {
          intros i Hlen.
          eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
          lia.
        }
        {
          intros ln len_n Hl_eq Hle.
          eapply List32_assign_list_in_bin_code_other in Heq; eauto.
        }
  - inversion Hthumb.
Qed.

Lemma jit_call_save_imm_v2_eq:
  forall l tp_len tp_bin a ld_set st_set ld_set1 st_set1
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HJIT : jit_call_save_imm_v1 a ld_set st_set = (l, ld_set1, st_set1)),
      exists tp_bin1,
      jit_call_save_imm_v2 a ld_set st_set tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1, ld_set1, st_set1) /\
      list_in_bin_code l tp_len tp_bin1 = true /\
      List.length tp_bin = List.length tp_bin1 /\
      (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
      (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
        len_n + List.length ln <= tp_len ->
        list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_call_save_imm_v1, jit_call_save_imm_v2; intros.
  injection HJIT as Hl_eq Hld_eq Hst_eq.
  eapply jit_call_save_add_v2_eq in Hl_eq; eauto.
  destruct Hl_eq as (tp1 & HJIT & Hl & Hlen & Hnth & Hlist).
  rewrite HJIT.
  exists tp1.
  subst.
  split; [f_equal | ].
  split; [assumption |].
  split; [assumption |].
  split; assumption.
Qed.

Lemma bpf_alu32_to_thumb_imm_comm_v2_eq:
  forall l op a b c tp_len tp_bin
    (HMAX : tp_len + Datatypes.length l < Datatypes.length tp_bin)
    (Hthumb : bpf_alu32_to_thumb_imm_comm op a b c = Some l),
      exists tp_bin1 : bin_code,
        bpf_alu32_to_thumb_imm_comm_v2 op a b c tp_len tp_bin =
          Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold bpf_alu32_to_thumb_imm_comm, bpf_alu32_to_thumb_imm_comm_v2; intros.
  destruct (_ && _).
  {
    inversion Hthumb; subst; clear Hthumb.
    unfold tp_bin_add; simpl in *.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l1 & Heq).
    rewrite Heq.
    exists l1.
    split; [reflexivity |].
    erewrite List32_assign_nth_error_same; eauto.
    rewrite Int.eq_true.
    split; [reflexivity| ].
    split.
    - eapply List32_assign_same_length; eauto.
    - split.
      + intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
        lia.
      + intros ln len_n Hl Hle.
        eapply List32_assign_list_in_bin_code_other; eauto.
  }

  destruct Int.eq.
  - destruct bpf_alu32_to_thumb_reg as [l1| ]eqn: Hreg;
      inversion Hthumb; subst; clear Hthumb.
    unfold tp_bin_add; simpl in *.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l2 & Heq).
    rewrite Heq.
    eapply List32_assign_same_length in Heq as Hlen2.
    eapply bpf_alu32_to_thumb_reg_v2_eq
      with (tp_len := tp_len + 1) (tp_bin := l2) in Hreg.
    2:{
      rewrite <- Hlen2.
      lia.
    }
    destruct Hreg as (tp1 & Hreg & Hl & Hlen & Hother & Hlist).
    rewrite Hlen2.
    exists tp1.
    rewrite Hreg.
    split; [f_equal; f_equal; lia |].
    rewrite <- Hlen.
    rewrite <- Hother; [| lia].
    eapply List32_assign_nth_error_same in Heq as Hnth.
    rewrite Hnth.
    rewrite Int.eq_true.
    split.
    + rewrite Nat.add_1_r in Hl.
      assumption.
    + split; [ f_equal |].
      split.
      * intros i Hle.
        erewrite <- Hother; eauto; [| lia].
        eapply List32_assign_nth_error_other with (ofs' := i) in Heq; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply Hlist. 2:{ lia. }
        eapply List32_assign_list_in_bin_code_other; eauto.
  - destruct bpf_alu32_to_thumb_reg as [l1| ]eqn: Hreg;
      inversion Hthumb; subst; clear Hthumb.
    unfold tp_bin_add; simpl in *.
    assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l2 & Hl2_eq).
    rewrite Hl2_eq.
    eapply List32_assign_same_length in Hl2_eq as Hlen2.
    assert (Heq: tp_len + 1 < Datatypes.length l2) by lia.
    eapply List32_assign_Some in Heq.
    destruct Heq as (l3 & Hl3_eq).
    rewrite Hl3_eq.
    eapply List32_assign_same_length in Hl3_eq as Hlen3.
    eapply bpf_alu32_to_thumb_reg_v2_eq
      with (tp_len := tp_len + 2) (tp_bin := l3) in Hreg.
    2:{
      rewrite <- Hlen3.
      lia.
    }
    destruct Hreg as (tp1 & Hreg & Hl & Hlen & Hother & Hlist).
    rewrite Hlen2.
    rewrite Hlen3.
    exists tp1.
    rewrite Hreg.
    split; [f_equal; f_equal; lia |].
    rewrite <- Hlen.
    rewrite <- Hother; [| lia].
    eapply List32_assign_nth_error_same in Hl2_eq as Heq2.
    eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl3_eq as Heq3.
    2:{ lia. }
    rewrite <- Heq3; clear Heq3.
    rewrite Heq2.
    rewrite Int.eq_true.
    eapply List32_assign_nth_error_same in Hl3_eq as Heq3.
    rewrite Hother in Heq3.
    2:{ lia. }
    rewrite Nat.add_1_r in Heq3.
    simpl in Heq3.
    rewrite Heq3.
    rewrite Int.eq_true.
    split.
    + assert (Heq: S (S tp_len) = tp_len + 2) by lia.
      rewrite Heq.
      assumption.
    + split; [ f_equal |].
      split.
      * intros i Hle.
        erewrite <- Hother; eauto; [| lia].
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl3_eq; eauto; [| lia].
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl2_eq; eauto; [| lia].
        rewrite Hl2_eq.
        assumption.
      * intros ln len_n Hl_t Hle.
        eapply Hlist. 2:{ lia. }
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl3_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl2_eq; eauto.
Qed.

Lemma bpf_alu32_to_thumb_imm_shift_comm_v2_eq:
  forall l op a b tp_len tp_bin
    (HMAX : tp_len + Datatypes.length l < Datatypes.length tp_bin)
    (Hthumb : bpf_alu32_to_thumb_imm_shift_comm op a b = Some l),
      exists tp_bin1 : bin_code,
        bpf_alu32_to_thumb_imm_shift_comm_v2 op a b tp_len tp_bin =
          Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold bpf_alu32_to_thumb_imm_shift_comm, bpf_alu32_to_thumb_imm_shift_comm_v2; intros.
  destruct (_ && _); [| inversion Hthumb].
  inversion Hthumb; subst; clear Hthumb.
  unfold tp_bin_add; simpl in *.
  assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
  eapply List32_assign_Some in Heq.
  destruct Heq as (l1 & Hl1_eq).
  rewrite Hl1_eq.
  eapply List32_assign_same_length in Hl1_eq as Hlen1_eq; eauto.
  assert (Heq: tp_len + 1 < Datatypes.length l1) by lia.
  eapply List32_assign_Some in Heq.
  destruct Heq as (l2 & Hl2_eq).
  rewrite Hl2_eq.
  eapply List32_assign_same_length in Hl2_eq as Hlen2_eq; eauto.
  exists l2.
  split; [reflexivity |].
  eapply List32_assign_nth_error_same in Hl1_eq as Heq1; eauto.
  eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl2_eq as Heq2; eauto.
  2:{ lia. }
  rewrite <- Heq2; clear Heq2.
  rewrite Heq1.
  rewrite Int.eq_true.
  rewrite Nat.add_1_r in Hl2_eq.
  eapply List32_assign_nth_error_same in Hl2_eq as Heq2; eauto.
  simpl in Heq2.
  rewrite Heq2.
  rewrite Int.eq_true.
  split; [reflexivity| ].
  split.
  - rewrite Hlen1_eq.
    assumption.
  - split.
    + intros i Hle.
      eapply List32_assign_nth_error_other with (ofs' := i) in Hl2_eq; eauto; [| lia].
      rewrite <- Hl2_eq.
      eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto.
      lia.
    + intros ln len_n Hl_t Hle.
      eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl2_eq; eauto.
      eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.
Qed.

Lemma bpf_alu32_to_thumb_imm_v2_eq:
  forall l a b c tp_len tp_bin
    (HMAX : tp_len + Datatypes.length l < Datatypes.length tp_bin)
    (Hthumb : bpf_alu32_to_thumb_imm a b c = Some l),
      exists tp_bin1 : bin_code,
        bpf_alu32_to_thumb_imm_v2 a b c tp_len tp_bin =
          Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold bpf_alu32_to_thumb_imm, bpf_alu32_to_thumb_imm_v2; intros.
  destruct a.
  - eapply bpf_alu32_to_thumb_imm_comm_v2_eq with
    (tp_len := tp_len) (tp_bin := tp_bin) in Hthumb; eauto.
  - eapply bpf_alu32_to_thumb_imm_comm_v2_eq with
    (tp_len := tp_len) (tp_bin := tp_bin) in Hthumb; eauto.
  - destruct Int.eq eqn: Hc_eq.
    + destruct bpf_alu32_to_thumb_reg as [lr | ] eqn: Hreg;
        inversion Hthumb; subst; clear Hthumb.
      simpl in HMAX.
      unfold tp_bin_add.
      assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l1 & Hl1_eq).
      rewrite Hl1_eq.
      eapply List32_assign_same_length in Hl1_eq as Hlen_eq.
      eapply bpf_alu32_to_thumb_reg_v2_eq with
      (tp_len := tp_len + 1) (tp_bin := l1) in Hreg; eauto.
      2:{ rewrite <- Hlen_eq.
          lia.
      }
      destruct Hreg as (tp1 & Hreg & Hl & Hlen & Hother & Hlist).
      exists tp1.
      rewrite Hreg.
      split; [simpl; f_equal; f_equal; lia |].
      rewrite Hlen in Hlen_eq.
      split.
      {
        simpl.
        erewrite <- Hother.
        2:{ lia. }

        eapply List32_assign_nth_error_same in Hl1_eq.
        rewrite Hl1_eq.
        rewrite Int.eq_true.
        rewrite Nat.add_1_r in Hl.
        assumption.
      }
      split; [assumption |].
      split.
      * intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto; [| lia].
        rewrite Hl1_eq.
        eapply Hother; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply Hlist. 2:{ lia. }
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.

    + destruct bpf_alu32_to_thumb_reg as [lr | ] eqn: Hreg;
        inversion Hthumb; subst; clear Hthumb.
      simpl in HMAX.
      unfold tp_bin_add.
      assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l1 & Hl1_eq).
      rewrite Hl1_eq.
      eapply List32_assign_same_length in Hl1_eq as Hlen1_eq.
      assert (Heq: tp_len + 1 < Datatypes.length l1) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l2 & Hl2_eq).
      rewrite Hl2_eq.
      eapply List32_assign_same_length in Hl2_eq as Hlen2_eq.
      eapply bpf_alu32_to_thumb_reg_v2_eq with
      (tp_len := tp_len + 2) (tp_bin := l2) in Hreg; eauto.
      2:{ rewrite <- Hlen2_eq.
          lia.
      }
      destruct Hreg as (tp1 & Hreg & Hl & Hlen & Hother & Hlist).
      exists tp1.
      rewrite Hreg.
      split; [simpl; f_equal; f_equal; lia |].
      rewrite Hlen in Hlen2_eq.
      rewrite <- Hlen1_eq in Hlen2_eq.
      split.
      {
        simpl.
        erewrite <- Hother.
        2:{ lia. }

        eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl2_eq as Heq.
        2:{ lia. }
        rewrite <- Heq; clear Heq.
        eapply List32_assign_nth_error_same in Hl1_eq as Heq.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.

        eapply List32_assign_nth_error_same in Hl2_eq as Heq.
        rewrite Nat.add_1_r in Heq.
        rewrite Hother in Heq.
        2:{ lia. }
        simpl in Heq.
        rewrite Heq; clear Heq.

        rewrite Int.eq_true.
        assert (Heq: S (S tp_len) = tp_len + 2) by lia.
        rewrite Heq.
        assumption.
      }
      split; [assumption |].
      split.
      * intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto; [| lia].
        rewrite Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl2_eq; eauto; [| lia].
        rewrite Hl2_eq.
        eapply Hother; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply Hlist. 2:{ lia. }
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl2_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.

  - destruct Int.eq eqn: Hc_eq; [inversion Hthumb |].
    destruct Int.eq eqn: Hc1_eq in Hthumb.
    + rewrite Hc1_eq.
      inversion Hthumb; subst; clear Hthumb.
      simpl in HMAX.
      unfold tp_bin_add.
      assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l1 & Hl1_eq).
      rewrite Hl1_eq.
      eapply List32_assign_same_length in Hl1_eq as Hlen1_eq.
      assert (Heq: tp_len + 1 < Datatypes.length l1) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l2 & Hl2_eq).
      rewrite Hl2_eq.
      exists l2.
      split; [simpl; f_equal; f_equal; lia |].
      eapply List32_assign_same_length in Hl2_eq as Hlen2_eq.
      rewrite <- Hlen2_eq.
      split.
      {
        simpl.

        eapply List32_assign_nth_error_same in Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl2_eq as Heq.
        2:{ lia. }
        rewrite <- Heq; clear Heq.
        rewrite Hl1_eq.
        rewrite Int.eq_true.
        rewrite Nat.add_1_r in Hl2_eq.
        eapply List32_assign_nth_error_same in Hl2_eq.
        simpl in Hl2_eq.
        rewrite Hl2_eq.
        rewrite Int.eq_true.
        reflexivity.
      }
      split; [assumption |].
      split.
      * intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto; [| lia].
        rewrite Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl2_eq; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl2_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.
    + rewrite Hc1_eq.
      inversion Hthumb; subst; clear Hthumb.
      simpl in HMAX.
      unfold tp_bin_add.
      assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l1 & Hl1_eq).
      rewrite Hl1_eq.
      eapply List32_assign_same_length in Hl1_eq as Hlen1_eq.
      assert (Heq: tp_len + 1 < Datatypes.length l1) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l2 & Hl2_eq).
      rewrite Hl2_eq.
      eapply List32_assign_same_length in Hl2_eq as Hlen2_eq.
      assert (Heq: tp_len + 2 < Datatypes.length l2) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l3 & Hl3_eq).
      rewrite Hl3_eq.
      exists l3.
      split; [simpl; f_equal; f_equal; lia |].
      eapply List32_assign_same_length in Hl3_eq as Hlen3_eq.
      rewrite <- Hlen3_eq.
      rewrite <- Hlen2_eq.
      split.
      {
        simpl.

        eapply List32_assign_nth_error_same in Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl2_eq as Heq.
        2:{ lia. }
        rewrite Hl1_eq in Heq.
        eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl3_eq as Heq1.
        rewrite Heq1 in Heq.
        rewrite <- Heq; clear Heq Heq1.
        rewrite Int.eq_true.

        eapply List32_assign_nth_error_same in Hl2_eq as Heq.
        rewrite Nat.add_1_r in Heq.
        2:{ lia. }
        eapply List32_assign_nth_error_other with (ofs' := S tp_len) in Hl3_eq as Heq1.
        2:{ lia. }
        rewrite Heq in Heq1.
        simpl in Heq1.
        rewrite <- Heq1; clear Heq Heq1.

        assert (Heq: tp_len + 2 = S (S tp_len)) by lia.
        rewrite Heq in Hl3_eq.
        eapply List32_assign_nth_error_same in Hl3_eq.
        simpl in Hl3_eq.
        rewrite Hl3_eq.
        rewrite Int.eq_true.
        rewrite Int.eq_true.
        reflexivity.
      }
      split; [assumption |].
      split.
      * intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto; [| lia].
        rewrite Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl2_eq; eauto; [| lia].
        rewrite Hl2_eq.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl3_eq; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl3_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl2_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.

  - eapply bpf_alu32_to_thumb_imm_comm_v2_eq with
    (tp_len := tp_len) (tp_bin := tp_bin) in Hthumb; eauto.
  - eapply bpf_alu32_to_thumb_imm_comm_v2_eq with
    (tp_len := tp_len) (tp_bin := tp_bin) in Hthumb; eauto.
  - eapply bpf_alu32_to_thumb_imm_shift_comm_v2_eq with
    (tp_len := tp_len) (tp_bin := tp_bin) in Hthumb; eauto.
  - eapply bpf_alu32_to_thumb_imm_shift_comm_v2_eq with
    (tp_len := tp_len) (tp_bin := tp_bin) in Hthumb; eauto.
  - destruct Int.eq eqn: Hc_eq.
    + inversion Hthumb; subst; clear Hthumb.
      simpl in HMAX.
      unfold tp_bin_add.
      assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l1 & Hl1_eq).
      rewrite Hl1_eq.
      eapply List32_assign_same_length in Hl1_eq as Hlen1_eq.
      assert (Heq: tp_len + 1 < Datatypes.length l1) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l2 & Hl2_eq).
      rewrite Hl2_eq.
      eapply List32_assign_same_length in Hl2_eq as Hlen2_eq.

      exists l2.
      split; [simpl; f_equal; f_equal; lia |].
      rewrite Hlen1_eq.
      split.
      {
        simpl.

        eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl2_eq as Heq.
        2:{ lia. }
        rewrite <- Heq; clear Heq.
        eapply List32_assign_nth_error_same in Hl1_eq as Heq.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.

        eapply List32_assign_nth_error_same in Hl2_eq as Heq.
        rewrite Nat.add_1_r in Heq.
        simpl in Heq.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.
        reflexivity.
      }
      split; [assumption |].
      split.
      * intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto; [| lia].
        rewrite Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl2_eq; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl2_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.

    + inversion Hthumb; subst; clear Hthumb.
      simpl in HMAX.
      unfold tp_bin_add.
      assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l1 & Hl1_eq).
      rewrite Hl1_eq.
      eapply List32_assign_same_length in Hl1_eq as Hlen1_eq.
      assert (Heq: tp_len + 1 < Datatypes.length l1) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l2 & Hl2_eq).
      rewrite Hl2_eq.
      eapply List32_assign_same_length in Hl2_eq as Hlen2_eq.
      assert (Heq: tp_len + 2 < Datatypes.length l2) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l3 & Hl3_eq).
      rewrite Hl3_eq.
      eapply List32_assign_same_length in Hl3_eq as Hlen3_eq.

      exists l3.
      split; [simpl; f_equal; f_equal; lia |].
      rewrite Hlen1_eq.
      rewrite Hlen2_eq.
      split.
      {
        assert (Heq: S (S tp_len) = tp_len + 2) by lia.
        rewrite <- Heq in Hl3_eq; clear Heq.
        simpl.

        eapply List32_assign_nth_error_same in Hl1_eq as Heq.
        eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl2_eq as Heq1.
        2:{ lia. }
        rewrite Heq1 in Heq; clear Heq1.
        eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl3_eq as Heq1.
        2:{ lia. }
        rewrite Heq1 in Heq; clear Heq1.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.

        eapply List32_assign_nth_error_same in Hl2_eq as Heq.
        rewrite Nat.add_1_r in Heq.
        eapply List32_assign_nth_error_other with (ofs' := S tp_len) in Hl3_eq as Heq1.
        2:{ lia. }
        rewrite Heq1 in Heq; clear Heq1.
        simpl in Heq.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.

        eapply List32_assign_nth_error_same in Hl3_eq as Heq.
        simpl in Heq.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.
        reflexivity.
      }
      split; [assumption |].
      split.
      * intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto; [| lia].
        rewrite Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl2_eq; eauto; [| lia].
        rewrite Hl2_eq.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl3_eq; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl3_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl2_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.
  - inversion Hthumb.
  - eapply bpf_alu32_to_thumb_imm_comm_v2_eq with
    (tp_len := tp_len) (tp_bin := tp_bin) in Hthumb; eauto.
  - destruct Int.eq eqn: Hc_eq.
    + inversion Hthumb; subst; clear Hthumb.
      simpl in HMAX.
      unfold tp_bin_add.
      assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l1 & Hl1_eq).
      rewrite Hl1_eq.
      eapply List32_assign_same_length in Hl1_eq as Hlen1_eq.

      exists l1.
      split; [simpl; f_equal; f_equal; lia |].
      split.
      {
        simpl.

        eapply List32_assign_nth_error_same in Hl1_eq as Heq.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.
        reflexivity.
      }
      split; [assumption |].
      split.
      * intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.

    + inversion Hthumb; subst; clear Hthumb.
      simpl in HMAX.
      unfold tp_bin_add.
      assert (Heq: tp_len < Datatypes.length tp_bin) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l1 & Hl1_eq).
      rewrite Hl1_eq.
      eapply List32_assign_same_length in Hl1_eq as Hlen1_eq.
      assert (Heq: tp_len + 1 < Datatypes.length l1) by lia.
      eapply List32_assign_Some in Heq.
      destruct Heq as (l2 & Hl2_eq).
      rewrite Hl2_eq.
      eapply List32_assign_same_length in Hl2_eq as Hlen2_eq.

      exists l2.
      split; [simpl; f_equal; f_equal; lia |].
      rewrite Hlen1_eq.
      split.
      {
        simpl.
        eapply List32_assign_nth_error_same in Hl1_eq as Heq.
        eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl2_eq as Heq1.
        2:{ lia. }
        rewrite Heq1 in Heq; clear Heq1.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.

        eapply List32_assign_nth_error_same in Hl2_eq as Heq.
        rewrite Nat.add_1_r in Heq.
        simpl in Heq.
        rewrite Heq; clear Heq.
        rewrite Int.eq_true.
        reflexivity.
      }
      split; [assumption |].
      split.
      * intros i Hle.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl1_eq; eauto; [| lia].
        rewrite Hl1_eq.
        eapply List32_assign_nth_error_other with (ofs' := i) in Hl2_eq; eauto.
        lia.
      * intros ln len_n Hl_t Hle.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl2_eq; eauto.
        lia.
        eapply List32_assign_list_in_bin_code_other with (len1 := len_n) in Hl1_eq; eauto.

  - eapply bpf_alu32_to_thumb_imm_shift_comm_v2_eq with
    (tp_len := tp_len) (tp_bin := tp_bin) in Hthumb; eauto.
Qed.

Lemma jit_one_v2_eq:
  forall l tp_len tp_bin a b s ld_set1 st_set1 ld1 st1
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (Hone : jit_one_v1 a b s ld_set1 st_set1 = Some (l, ld1, st1)),
      exists tp_bin1,
        jit_one_v2 a b s ld_set1 st_set1 tp_len tp_bin =
          Some (tp_len + List.length l, tp_bin1, ld1, st1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_one_v1, jit_one_v2.
  intros.
  destruct s.
  - destruct jit_call_save_reg_v1 as ((l3 & ld_set3) & st_set3) eqn: Hreg.
    destruct bpf_alu32_to_thumb_reg as [l4 |] eqn: Hthumb; [| inversion Hone].
    injection Hone as Hl1_eq Hld1_eq Hst1_eq.
    subst l ld1 st1.
    rewrite app_length in HMAX.
    eapply jit_call_save_reg_v2_eq with (tp_len := tp_len) (tp_bin := tp_bin) in Hreg.
    2:{
      lia.
    }
    destruct Hreg as (tp1 & HJIT & Hlist & Hlen1_eq & Hother1 & Hlt1).
    rewrite HJIT.

    eapply bpf_alu32_to_thumb_reg_v2_eq with (tp_len := tp_len + Datatypes.length l3)
      (tp_bin := tp1) in Hthumb.
    2:{
      lia.
    }
    destruct Hthumb as (tp2 & Hreg & Hlist2 & Hlen2_eq & Hother2 & Hlt2).
    rewrite Hreg.
    exists tp2.
    split.
    + rewrite app_length.
      rewrite Nat.add_assoc.
      f_equal.
    + split.
      * rewrite list_in_bin_code_concat_iff.
        split; [ | assumption].
        assert (Heq: tp_len + Datatypes.length l3 + 0 =
          tp_len + Datatypes.length l3) by lia.
        rewrite <- Heq in Hreg.
        eapply Hlt2; eauto.
      * split.
        {
          rewrite Hlen1_eq.
          assumption.
        }
        split.
        {
          intros i Hle.
          rewrite Hother1; [| lia].
          rewrite Hother2; auto.
          lia.
        }
        {
          intros ln len_n Hlt Hle.
          apply Hlt2; [| lia].
          apply Hlt1; auto.
        }
  - destruct jit_call_save_imm_v1 as ((l3 & ld_set3) & st_set3) eqn: Himm.
    destruct bpf_alu32_to_thumb_imm as [l4 |] eqn: Hthumb; [| inversion Hone].
    injection Hone as Hl1_eq Hld1_eq Hst1_eq.
    subst l ld1 st1.
    rewrite app_length in HMAX.
    eapply jit_call_save_imm_v2_eq with (tp_len := tp_len) (tp_bin := tp_bin) in Himm.
    2:{
      lia.
    }
    destruct Himm as (tp1 & HJIT & Hlist & Hlen1_eq & Hother1 & Hlt1).
    rewrite HJIT.

    eapply bpf_alu32_to_thumb_imm_v2_eq with (tp_len := tp_len + Datatypes.length l3)
      (tp_bin := tp1) in Hthumb.
    2:{
      lia.
    }
    destruct Hthumb as (tp2 & Hreg & Hlist2 & Hlen2_eq & Hother2 & Hlt2).
    rewrite Hreg.
    exists tp2.
    split.
    + rewrite app_length.
      rewrite Nat.add_assoc.
      f_equal.
    + split.
      * rewrite list_in_bin_code_concat_iff.
        split; [ | assumption].
        eapply Hlt2; eauto.
      * split.
        {
          rewrite Hlen1_eq.
          assumption.
        }
        split.
        {
          intros i0 Hle.
          rewrite Hother1; [| lia].
          rewrite Hother2; auto.
          lia.
        }
        {
          intros ln len_n Hlt Hle.
          apply Hlt2; [| lia].
          apply Hlt1; auto.
        }
Qed.

Lemma get_alu32_jit_list4_eq:
  forall fuel l pc c counter ld_set1 st_set1 ld_set2 st_set2
    n tp_len tp_bin
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (Hjit: get_alu32_jit_list_v3 c fuel pc counter ld_set1 st_set1 =
            Some (l, ld_set2, st_set2, n)),
      exists tp_bin1,
      get_alu32_jit_list_v4 c fuel pc counter ld_set1 st_set1 tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1, ld_set2, st_set2, n) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  induction fuel; simpl; intros.
  { inversion Hjit. }

  destruct nth_error as [ins32 |] eqn: Hnth; [| inversion Hjit].
  destruct decode_ind as [ins |] eqn: Hdecode; [| inversion Hjit].
  destruct ins; try (
    inversion Hjit; subst; clear Hjit;
    simpl; rewrite Nat.add_0_r;
    eexists;
      split; [f_equal |
      split; [reflexivity|
      split; [reflexivity|
      split; intros; [reflexivity| assumption]]]]).

  destruct jit_one_v1 as [((l1 & ld1) & st1)|] eqn: Hone; [| inversion Hjit].
  destruct get_alu32_jit_list_v3 as [(((l2 & ld2) & st2) & n2)|]
    eqn: Halu32; inversion Hjit; subst; clear Hjit.
  rewrite List.app_length in HMAX.
  eapply jit_one_v2_eq with (tp_len := tp_len) (tp_bin := tp_bin) in Hone.
  2:{ lia. }
  destruct Hone as (tp_lenk & Hone & Hl & Hlen & Hother & Hlist).
  rewrite Hone.
  eapply IHfuel with (tp_len := tp_len + Datatypes.length l1) (tp_bin := tp_lenk) in Halu32; eauto.
  2:{ lia. }
  destruct Halu32 as (tp3 & Hget & Hl3 & Hlen3 & Hother3 & Hlist3).
  exists tp3.
  rewrite List.app_length.
  split.
  - rewrite Nat.add_assoc.
    assumption.
  - split.
    + rewrite list_in_bin_code_concat_iff.
      split; [| assumption].
      eapply Hlist3; eauto.
    + split.
      * rewrite Hlen.
        assumption.
      * split.
        {
          intros i Hle.
          erewrite Hother; [| lia].
          rewrite Hother3; auto.
          lia.
        }
        {
          intros ln len_n Hln Hlen_eq.
          eapply Hlist3; [| lia].
          eapply Hlist; eauto.
        }
Qed.

Lemma jit_alu32_pre_v2_eq:
  forall tp_len tp_bin
    (HMAX: tp_len + List.length jit_alu32_pre < List.length tp_bin),
      exists tp_bin1,
      jit_alu32_pre_v2 tp_len tp_bin =
        Some (tp_len + List.length jit_alu32_pre, tp_bin1) /\
        list_in_bin_code jit_alu32_pre tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_alu32_pre, jit_alu32_pre_v2; intros.
  unfold tp_bin_add; simpl in HMAX.
  assert (Heq0: tp_len < Datatypes.length tp_bin) by lia.
  eapply List32_assign_Some in Heq0.
  destruct Heq0 as (l0 & Hl0_eq).
  rewrite Hl0_eq.

  apply List32_assign_same_length in Hl0_eq as Heq.
  assert (Heq1: S tp_len < Datatypes.length l0) by lia.
  eapply List32_assign_Some in Heq1.
  destruct Heq1 as (l1 & Hl1_eq).
  rewrite Hl1_eq.
  eexists; split; [simpl; reflexivity |].

  simpl.
  eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
  eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl1_eq as Heq2; eauto.
  rewrite <- Heq2.
  rewrite Heq1.
  rewrite Int.eq_true.
  clear Heq Heq1 Heq2.
  eapply List32_assign_nth_error_same in Hl1_eq as Heq.
  destruct l1.
  - simpl in Heq.
    inversion Heq.
  - simpl in Heq.
    rewrite Heq.
    rewrite Int.eq_true.
    split; [reflexivity| ].
    eapply List32_assign_same_length in Hl0_eq as Heq0.
    eapply List32_assign_same_length in Hl1_eq as Heq1.
    rewrite Heq0.
    split; [ assumption |].
    split.
    + intros i0 Hle.
      eapply List32_assign_nth_error_other with (ofs' := i0) in Hl1_eq;
        eauto; [| lia].
      rewrite <- Hl1_eq.
      eapply List32_assign_nth_error_other with (ofs' := i0) in Hl0_eq;
        eauto.
      lia.
    + intros ln len_n Hln_eq Hle.
      eapply List32_assign_list_in_bin_code_other
        with (len1 := len_n) in Hl1_eq; eauto.
      eapply List32_assign_list_in_bin_code_other
        with (len1 := len_n) in Hl0_eq; eauto.
Qed.

Lemma jit_alu32_thumb_pc_add_v2_eq:
  forall tp_len tp_bin l n
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HPC: jit_alu32_thumb_pc_add n = Some l),
      exists tp_bin1,
      jit_alu32_thumb_pc_add_v2 n tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_alu32_thumb_pc_add, jit_alu32_thumb_pc_add_v2; intros.
  destruct (_ && _); [| inversion HPC].
  injection HPC as Heq; subst l; simpl in HMAX.
  unfold tp_bin_add.
  assert (Heq0: tp_len < Datatypes.length tp_bin) by lia.
  eapply List32_assign_Some in Heq0.
  destruct Heq0 as (l0 & Hl0_eq).
  rewrite Hl0_eq.
  apply List32_assign_same_length in Hl0_eq as Heq.

  eexists; split; [simpl; reflexivity |].

  simpl.
  eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
  rewrite Heq1.
  rewrite Int.eq_true.
  clear Heq Heq1.
  split; [reflexivity| ].
  eapply List32_assign_same_length in Hl0_eq as Heq0.
  split; [ assumption |].
  split.
  - intros i0 Hle.
    eapply List32_assign_nth_error_other with (ofs' := i0) in Hl0_eq;
      eauto.
    lia.
  - intros ln len_n Hln_eq Hle.
    eapply List32_assign_list_in_bin_code_other
      with (len1 := len_n) in Hl0_eq; eauto.
Qed.

Lemma jit_alu32_thumb_pc_v2_eq:
  forall tp_len tp_bin l n
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HPC: jit_alu32_thumb_pc n = Some l),
      exists tp_bin1,
      jit_alu32_thumb_pc_v2 n tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_alu32_thumb_pc, jit_alu32_thumb_pc_v2; intros.
  destruct jit_alu32_thumb_pc_add as [l_add| ] eqn: Hadd; [| inversion HPC].
  injection HPC as Heq; subst l.
  unfold tp_bin_add; simpl in HMAX.
  assert (Heq0: tp_len < Datatypes.length tp_bin) by lia.
  eapply List32_assign_Some in Heq0.
  destruct Heq0 as (l0 & Hl0_eq).
  rewrite Hl0_eq.
  apply List32_assign_same_length in Hl0_eq as Heq.

  rewrite Nat.add_1_r.
  rewrite <- Nat.add_succ_comm in HMAX.
  rewrite List.app_length in HMAX.
  eapply jit_alu32_thumb_pc_add_v2_eq with
    (tp_len := S tp_len) (tp_bin := l0) in Hadd.
  2:{
    lia.
  }
  destruct Hadd as (tp1 & Hadd & Hlist_add & Hlen_add & Hother_add & Hl_add).
  rewrite Hadd.


  assert (Heq1: S tp_len + Datatypes.length l_add < Datatypes.length tp1) by lia.
  eapply List32_assign_Some in Heq1.
  destruct Heq1 as (l1 & Hl1_eq).
  rewrite Hl1_eq.
  eexists; split; [simpl; rewrite List.app_length; simpl |].
  { f_equal. f_equal. lia. }

  simpl.
  eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
  eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl1_eq as Heq2; eauto.
  2:{ lia. }
  rewrite <- Heq2.
  rewrite <- Hother_add.
  2:{ lia. }
  rewrite Heq1.
  rewrite Int.eq_true.
  clear Heq Heq1 Heq2.

  split.
  - rewrite list_in_bin_code_concat_iff.
    simpl; split.
    + eapply List32_assign_list_in_bin_code_other
        with (len1 := S tp_len) (l1 := l_add) in Hl1_eq as Heq; eauto.
    + eapply List32_assign_nth_error_same in Hl1_eq.
      rewrite plus_Sn_m in Hl1_eq.
      simpl in Hl1_eq.
      rewrite Hl1_eq.
      rewrite Int.eq_true.
      reflexivity.
  - eapply List32_assign_same_length in Hl0_eq as Heq0.
    eapply List32_assign_same_length in Hl1_eq as Heq1.
    rewrite Heq0.
    rewrite <- Heq1.
    split; [ assumption |].
    split.
    + intros i0 Hle.
      eapply List32_assign_nth_error_other with (ofs' := i0) in Hl1_eq;
        eauto; [| lia].
      rewrite <- Hl1_eq.
      rewrite <- Hother_add; [| lia].
      eapply List32_assign_nth_error_other with (ofs' := i0) in Hl0_eq;
        eauto.
      lia.
    + intros ln len_n Hln_eq Hle.
      eapply List32_assign_list_in_bin_code_other
        with (len1 := len_n) (l1 := ln) in Hl1_eq.
      * assumption.
      * lia.
      * eapply Hl_add.
        {
          eapply List32_assign_list_in_bin_code_other
            with (len1 := len_n) in Hl0_eq; eauto.
        }
        lia.
Qed.

Lemma jit_alu32_thumb_upd_store_eq:
  forall tp_len tp_bin l st r
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HPC: ThumbJIT1.jit_alu32_thumb_upd_store r st = l),
      exists tp_bin1,
      jit_alu32_thumb_upd_store r st tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold ThumbJIT1.jit_alu32_thumb_upd_store, jit_alu32_thumb_upd_store; intros.

  destruct eval_listset_regmap.
  2: {
    subst l.
    simpl in *.
    rewrite Nat.add_0_r in *.
    eexists; split; [reflexivity |].
    split; [reflexivity |].
    split; [reflexivity |].
    split.
    - intros; reflexivity.
    - intros; assumption.
  }

  subst l.
  unfold tp_bin_add; simpl in HMAX.
  assert (Heq0: tp_len < Datatypes.length tp_bin) by lia.
  eapply List32_assign_Some in Heq0.
  destruct Heq0 as (l0 & Hl0_eq).
  rewrite Hl0_eq.
  apply List32_assign_same_length in Hl0_eq as Heq.

  eexists; split; [simpl; reflexivity |].
  simpl.
  eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
  rewrite Heq1.
  rewrite Int.eq_true.
  split; [reflexivity |].
  split; [assumption |].
  split.
  - intros i0 Hle.
    eapply List32_assign_nth_error_other with (ofs' := i0) in Hl0_eq;
      eauto.
    lia.
  - intros ln len_n Hln_eq Hle.
    eapply List32_assign_list_in_bin_code_other
          with (len1 := len_n) in Hl0_eq; eauto.
Qed.

Lemma jit_alu32_thumb_store_v2_eq:
  forall tp_len tp_bin l st
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HPC: jit_alu32_thumb_store_v1 st = l),
      exists tp_bin1,
      jit_alu32_thumb_store_v2 st tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_alu32_thumb_store_v1, jit_alu32_thumb_store_v2; intros.
  subst l.
  rewrite ! List.app_length in HMAX.
  rewrite ! Nat.add_assoc in HMAX.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R0 st) as l_r0 eqn: Hr0_eq;
    symmetry in Hr0_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len) (tp_bin := tp_bin) in Hr0_eq.
  2:{ lia. }
  destruct Hr0_eq as (tp0 & Hr0 & Hl0 & Hlen0 & Hother0 & Hlist0).
  rewrite Hr0.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R1 st) as l_r1 eqn: Hr1_eq;
    symmetry in Hr1_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0) (tp_bin := tp0) in Hr1_eq.
  2:{ lia. }
  destruct Hr1_eq as (tp1 & Hr1 & Hl1 & Hlen1 & Hother1 & Hlist1).
  rewrite Hr1.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R2 st) as l_r2 eqn: Hr2_eq;
    symmetry in Hr2_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1)
        (tp_bin := tp1) in Hr2_eq.
  2:{ lia. }
  destruct Hr2_eq as (tp2 & Hr2 & Hl2 & Hlen2 & Hother2 & Hlist2).
  rewrite Hr2.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R3 st) as l_r3 eqn: Hr3_eq;
    symmetry in Hr3_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1
           + Datatypes.length l_r2)
        (tp_bin := tp2) in Hr3_eq.
  2:{ lia. }
  destruct Hr3_eq as (tp3 & Hr3 & Hl3 & Hlen3 & Hother3 & Hlist3).
  rewrite Hr3.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R4 st) as l_r4 eqn: Hr4_eq;
    symmetry in Hr4_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1
           + Datatypes.length l_r2 + Datatypes.length l_r3)
        (tp_bin := tp3) in Hr4_eq.
  2:{ lia. }
  destruct Hr4_eq as (tp4 & Hr4 & Hl4 & Hlen4 & Hother4 & Hlist4).
  rewrite Hr4.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R5 st) as l_r5 eqn: Hr5_eq;
    symmetry in Hr5_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1
           + Datatypes.length l_r2 + Datatypes.length l_r3 + Datatypes.length l_r4)
        (tp_bin := tp4) in Hr5_eq.
  2:{ lia. }
  destruct Hr5_eq as (tp5 & Hr5 & Hl5 & Hlen5 & Hother5 & Hlist5).
  rewrite Hr5.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R6 st) as l_r6 eqn: Hr6_eq;
    symmetry in Hr6_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1
           + Datatypes.length l_r2 + Datatypes.length l_r3 + Datatypes.length l_r4
           + Datatypes.length l_r5)
        (tp_bin := tp5) in Hr6_eq.
  2:{ lia. }
  destruct Hr6_eq as (tp6 & Hr6 & Hl6 & Hlen6 & Hother6 & Hlist6).
  rewrite Hr6.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R7 st) as l_r7 eqn: Hr7_eq;
    symmetry in Hr7_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1
           + Datatypes.length l_r2 + Datatypes.length l_r3 + Datatypes.length l_r4
           + Datatypes.length l_r5 + Datatypes.length l_r6)
        (tp_bin := tp6) in Hr7_eq.
  2:{ lia. }
  destruct Hr7_eq as (tp7 & Hr7 & Hl7 & Hlen7 & Hother7 & Hlist7).
  rewrite Hr7.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R8 st) as l_r8 eqn: Hr8_eq;
    symmetry in Hr8_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1
           + Datatypes.length l_r2 + Datatypes.length l_r3 + Datatypes.length l_r4
           + Datatypes.length l_r5 + Datatypes.length l_r6 + Datatypes.length l_r7)
        (tp_bin := tp7) in Hr8_eq.
  2:{ lia. }
  destruct Hr8_eq as (tp8 & Hr8 & Hl8 & Hlen8 & Hother8 & Hlist8).
  rewrite Hr8.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R9 st) as l_r9 eqn: Hr9_eq;
    symmetry in Hr9_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1
           + Datatypes.length l_r2 + Datatypes.length l_r3 + Datatypes.length l_r4
           + Datatypes.length l_r5 + Datatypes.length l_r6 + Datatypes.length l_r7
           + Datatypes.length l_r8)
        (tp_bin := tp8) in Hr9_eq.
  2:{ lia. }
  destruct Hr9_eq as (tp9 & Hr9 & Hl9 & Hlen9 & Hother9 & Hlist9).
  rewrite Hr9.

  remember (ThumbJIT1.jit_alu32_thumb_upd_store R10 st) as l_r10 eqn: Hr10_eq;
    symmetry in Hr10_eq.
  eapply jit_alu32_thumb_upd_store_eq
    with (tp_len := tp_len + Datatypes.length l_r0 + Datatypes.length l_r1
           + Datatypes.length l_r2 + Datatypes.length l_r3 + Datatypes.length l_r4
           + Datatypes.length l_r5 + Datatypes.length l_r6 + Datatypes.length l_r7
           + Datatypes.length l_r8 + Datatypes.length l_r9)
        (tp_bin := tp9) in Hr10_eq.
  2:{ lia. }
  destruct Hr10_eq as (tp10 & Hr10 & Hl10 & Hlen10 & Hother10 & Hlist10).
  rewrite Hr10.

  exists tp10.
  split.
  { rewrite ! List.app_length.
    rewrite ! Nat.add_assoc.
    f_equal.
  }

  split.
  {
    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      eapply Hlist6; [| lia].
      eapply Hlist5; [| lia].
      eapply Hlist4; [| lia].
      eapply Hlist3; [| lia].
      eapply Hlist2; [| lia].
      eapply Hlist1; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      eapply Hlist6; [| lia].
      eapply Hlist5; [| lia].
      eapply Hlist4; [| lia].
      eapply Hlist3; [| lia].
      eapply Hlist2; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      eapply Hlist6; [| lia].
      eapply Hlist5; [| lia].
      eapply Hlist4; [| lia].
      eapply Hlist3; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      eapply Hlist6; [| lia].
      eapply Hlist5; [| lia].
      eapply Hlist4; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      eapply Hlist6; [| lia].
      eapply Hlist5; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      eapply Hlist6; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      assumption.
    }
    assumption.
  }

  split.
  {
    rewrite Hlen0.
    rewrite Hlen1.
    rewrite Hlen2.
    rewrite Hlen3.
    rewrite Hlen4.
    rewrite Hlen5.
    rewrite Hlen6.
    rewrite Hlen7.
    rewrite Hlen8.
    rewrite Hlen9.
    assumption.
  }

  split.
  {
    intros i Hle.
    erewrite Hother0; [ |lia].
    erewrite Hother1; [ |lia].
    erewrite Hother2; [ |lia].
    erewrite Hother3; [ |lia].
    erewrite Hother4; [ |lia].
    erewrite Hother5; [ |lia].
    erewrite Hother6; [ |lia].
    erewrite Hother7; [ |lia].
    erewrite Hother8; [ |lia].
    erewrite Hother9; [ |lia].
    erewrite Hother10; [ |lia].
    reflexivity.
  }

  intros ln len_n HL Hle.
  eapply Hlist10; [| lia].
  eapply Hlist9; [| lia].
  eapply Hlist8; [| lia].
  eapply Hlist7; [| lia].
  eapply Hlist6; [| lia].
  eapply Hlist5; [| lia].
  eapply Hlist4; [| lia].
  eapply Hlist3; [| lia].
  eapply Hlist2; [| lia].
  eapply Hlist1; [| lia].
  eapply Hlist0; [| lia].
  assumption.
Qed.

Lemma jit_alu32_thumb_upd_reset_eq:
  forall tp_len tp_bin l st r
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HPC: ThumbJIT1.jit_alu32_thumb_upd_reset r st = l),
      exists tp_bin1,
      jit_alu32_thumb_upd_reset r st tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold ThumbJIT1.jit_alu32_thumb_upd_reset, jit_alu32_thumb_upd_reset; intros.

  destruct eval_listset_regmap.
  2: {
    subst l.
    simpl in *.
    rewrite Nat.add_0_r in *.
    eexists; split; [reflexivity |].
    split; [reflexivity |].
    split; [reflexivity |].
    split.
    - intros; reflexivity.
    - intros; assumption.
  }

  subst l.
  unfold tp_bin_add; simpl in HMAX.
  assert (Heq0: tp_len < Datatypes.length tp_bin) by lia.
  eapply List32_assign_Some in Heq0.
  destruct Heq0 as (l0 & Hl0_eq).
  rewrite Hl0_eq.
  apply List32_assign_same_length in Hl0_eq as Heq.

  eexists; split; [simpl; reflexivity |].
  simpl.
  eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
  rewrite Heq1.
  rewrite Int.eq_true.
  split; [reflexivity |].
  split; [assumption |].
  split.
  - intros i0 Hle.
    eapply List32_assign_nth_error_other with (ofs' := i0) in Hl0_eq;
      eauto.
    lia.
  - intros ln len_n Hln_eq Hle.
    eapply List32_assign_list_in_bin_code_other
          with (len1 := len_n) in Hl0_eq; eauto.
Qed.

Lemma jit_alu32_thumb_reset1_v2_eq:
  forall tp_len tp_bin l st
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HPC: jit_alu32_thumb_reset1_v1 st = l),
      exists tp_bin1,
      jit_alu32_thumb_reset1_v2 st tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_alu32_thumb_reset1_v1, jit_alu32_thumb_reset1_v2; intros.
  subst l.
  rewrite ! List.app_length in HMAX.
  rewrite ! Nat.add_assoc in HMAX.

  remember (ThumbJIT1.jit_alu32_thumb_upd_reset R4 st) as l_r4 eqn: Hr4_eq;
    symmetry in Hr4_eq.
  eapply jit_alu32_thumb_upd_reset_eq
    with (tp_len := tp_len) (tp_bin := tp_bin) in Hr4_eq.
  2:{ lia. }
  destruct Hr4_eq as (tp4 & Hr4 & Hl4 & Hlen4 & Hother4 & Hlist4).
  rewrite Hr4.

  remember (ThumbJIT1.jit_alu32_thumb_upd_reset R5 st) as l_r5 eqn: Hr5_eq;
    symmetry in Hr5_eq.
  eapply jit_alu32_thumb_upd_reset_eq
    with (tp_len := tp_len + Datatypes.length l_r4)
        (tp_bin := tp4) in Hr5_eq.
  2:{ lia. }
  destruct Hr5_eq as (tp5 & Hr5 & Hl5 & Hlen5 & Hother5 & Hlist5).
  rewrite Hr5.

  remember (ThumbJIT1.jit_alu32_thumb_upd_reset R6 st) as l_r6 eqn: Hr6_eq;
    symmetry in Hr6_eq.
  eapply jit_alu32_thumb_upd_reset_eq
    with (tp_len := tp_len + Datatypes.length l_r4
           + Datatypes.length l_r5)
        (tp_bin := tp5) in Hr6_eq.
  2:{ lia. }
  destruct Hr6_eq as (tp6 & Hr6 & Hl6 & Hlen6 & Hother6 & Hlist6).
  rewrite Hr6.

  remember (ThumbJIT1.jit_alu32_thumb_upd_reset R7 st) as l_r7 eqn: Hr7_eq;
    symmetry in Hr7_eq.
  eapply jit_alu32_thumb_upd_reset_eq
    with (tp_len := tp_len + Datatypes.length l_r4
           + Datatypes.length l_r5 + Datatypes.length l_r6)
        (tp_bin := tp6) in Hr7_eq.
  2:{ lia. }
  destruct Hr7_eq as (tp7 & Hr7 & Hl7 & Hlen7 & Hother7 & Hlist7).
  rewrite Hr7.

  remember (ThumbJIT1.jit_alu32_thumb_upd_reset R8 st) as l_r8 eqn: Hr8_eq;
    symmetry in Hr8_eq.
  eapply jit_alu32_thumb_upd_reset_eq
    with (tp_len := tp_len + Datatypes.length l_r4
           + Datatypes.length l_r5 + Datatypes.length l_r6 + Datatypes.length l_r7)
        (tp_bin := tp7) in Hr8_eq.
  2:{ lia. }
  destruct Hr8_eq as (tp8 & Hr8 & Hl8 & Hlen8 & Hother8 & Hlist8).
  rewrite Hr8.

  remember (ThumbJIT1.jit_alu32_thumb_upd_reset R9 st) as l_r9 eqn: Hr9_eq;
    symmetry in Hr9_eq.
  eapply jit_alu32_thumb_upd_reset_eq
    with (tp_len := tp_len + Datatypes.length l_r4
           + Datatypes.length l_r5 + Datatypes.length l_r6 + Datatypes.length l_r7
           + Datatypes.length l_r8)
        (tp_bin := tp8) in Hr9_eq.
  2:{ lia. }
  destruct Hr9_eq as (tp9 & Hr9 & Hl9 & Hlen9 & Hother9 & Hlist9).
  rewrite Hr9.

  remember (ThumbJIT1.jit_alu32_thumb_upd_reset R10 st) as l_r10 eqn: Hr10_eq;
    symmetry in Hr10_eq.
  eapply jit_alu32_thumb_upd_reset_eq
    with (tp_len := tp_len + Datatypes.length l_r4
           + Datatypes.length l_r5 + Datatypes.length l_r6 + Datatypes.length l_r7
           + Datatypes.length l_r8 + Datatypes.length l_r9)
        (tp_bin := tp9) in Hr10_eq.
  2:{ lia. }
  destruct Hr10_eq as (tp10 & Hr10 & Hl10 & Hlen10 & Hother10 & Hlist10).
  rewrite Hr10.

  exists tp10.
  split.
  { rewrite ! List.app_length.
    rewrite ! Nat.add_assoc.
    f_equal.
  }

  split.
  {
    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      eapply Hlist6; [| lia].
      eapply Hlist5; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      eapply Hlist6; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      eapply Hlist7; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      eapply Hlist8; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      eapply Hlist9; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hlist10; [| lia].
      assumption.
    }
    assumption.
  }

  split.
  {
    rewrite Hlen4.
    rewrite Hlen5.
    rewrite Hlen6.
    rewrite Hlen7.
    rewrite Hlen8.
    rewrite Hlen9.
    assumption.
  }

  split.
  {
    intros i Hle.
    erewrite Hother4; [ |lia].
    erewrite Hother5; [ |lia].
    erewrite Hother6; [ |lia].
    erewrite Hother7; [ |lia].
    erewrite Hother8; [ |lia].
    erewrite Hother9; [ |lia].
    erewrite Hother10; [ |lia].
    reflexivity.
  }

  intros ln len_n HL Hle.
  eapply Hlist10; [| lia].
  eapply Hlist9; [| lia].
  eapply Hlist8; [| lia].
  eapply Hlist7; [| lia].
  eapply Hlist6; [| lia].
  eapply Hlist5; [| lia].
  eapply Hlist4; [| lia].
  assumption.
Qed.

Lemma jit_alu32_thumb_reset_v2_eq:
  forall tp_len tp_bin l st
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (HPC: jit_alu32_thumb_reset_v1 st = l),
      exists tp_bin1,
      jit_alu32_thumb_reset_v2 st tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_alu32_thumb_reset_v1, jit_alu32_thumb_reset_v2; intros.
  subst l.

  rewrite ! List.app_length in HMAX.
  rewrite ! Nat.add_assoc in HMAX.

  unfold tp_bin_add.
  assert (Heq0: tp_len < Datatypes.length tp_bin) by lia.
  eapply List32_assign_Some in Heq0.
  destruct Heq0 as (l0 & Hl0_eq).
  rewrite Hl0_eq.
  apply List32_assign_same_length in Hl0_eq as Heq.

  simpl in HMAX.
  rewrite Heq in HMAX.
  eapply jit_alu32_thumb_reset1_v2_eq in HMAX as Heq1.
  2:{ reflexivity. }
  destruct Heq1 as (l1 & Hl1_eq & Hlist & Hlen & Hother & Hlt).
  exists l1.
  erewrite Hl1_eq.
  split. { simpl. f_equal. f_equal. lia. }
  simpl.
  rewrite <- Hother.
  2:{ lia. }

  eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
  rewrite Heq1.
  rewrite Int.eq_true.
  split.
  - rewrite Nat.add_1_r in Hlist.
    assumption.
  - split.
    + rewrite Heq.
      assumption.
    + split.
      * intros i Hle.
        rewrite <- Hother; [| lia].
        eapply List32_assign_nth_error_other in Hl0_eq; eauto.
        lia.
      * intros ln len_n HL Hle.
        apply Hlt; [| lia].
        eapply List32_assign_list_in_bin_code_other; eauto.
Qed.

Lemma jit_alu32_post_v2_eq:
  forall tp_len tp_bin
    (HMAX: tp_len + List.length jit_alu32_post < List.length tp_bin),
      exists tp_bin1,
      jit_alu32_post_v2 tp_len tp_bin =
        Some (tp_len + List.length jit_alu32_post, tp_bin1) /\
        list_in_bin_code jit_alu32_post tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold jit_alu32_post, jit_alu32_post_v2; intros.
  unfold tp_bin_add; simpl in HMAX.
  assert (Heq0: tp_len < Datatypes.length tp_bin) by lia.
  eapply List32_assign_Some in Heq0.
  destruct Heq0 as (l0 & Hl0_eq).
  rewrite Hl0_eq.

  apply List32_assign_same_length in Hl0_eq as Heq.
  assert (Heq1: S tp_len < Datatypes.length l0) by lia.
  eapply List32_assign_Some in Heq1.
  destruct Heq1 as (l1 & Hl1_eq).
  rewrite Nat.add_1_r.
  rewrite Hl1_eq.
  eexists; split; [simpl; reflexivity |].

  simpl.
  eapply List32_assign_nth_error_same in Hl0_eq as Heq1.
  eapply List32_assign_nth_error_other with (ofs' := tp_len) in Hl1_eq as Heq2; eauto.
  rewrite <- Heq2.
  rewrite Heq1.
  rewrite Int.eq_true.
  clear Heq Heq1 Heq2.
  eapply List32_assign_nth_error_same in Hl1_eq as Heq.
  simpl in Heq.
  rewrite Heq.
  rewrite Int.eq_true.
  split; [reflexivity| ].
  eapply List32_assign_same_length in Hl0_eq as Heq0.
  eapply List32_assign_same_length in Hl1_eq as Heq1.
  rewrite Heq0.
  split; [ assumption |].
  split.
  - intros i0 Hle.
    eapply List32_assign_nth_error_other with (ofs' := i0) in Hl1_eq;
      eauto; [| lia].
    rewrite <- Hl1_eq.
    eapply List32_assign_nth_error_other with (ofs' := i0) in Hl0_eq;
      eauto.
    lia.
  - intros ln len_n Hln_eq Hle.
    eapply List32_assign_list_in_bin_code_other
      with (len1 := len_n) in Hl1_eq; eauto.
    eapply List32_assign_list_in_bin_code_other
      with (len1 := len_n) in Hl0_eq; eauto.
Qed.

Lemma compose_jit_list_aux_v4_eq:
  forall fuel l pc c counter ld_set1 st_set1
    n tp_len tp_bin
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (Hjit: compose_jit_list_aux_v3 c fuel pc counter ld_set1 st_set1 = Some (l, n)),
      exists tp_bin1,
      compose_jit_list_aux_v4 c fuel pc counter ld_set1 st_set1 tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1, n) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold compose_jit_list_aux_v3, compose_jit_list_aux_v4; intros.
  destruct get_alu32_jit_list_v3 as [(((l1 & ld1) & st1) & n1) |]
    eqn: Hget; [| inversion Hjit].
  destruct jit_alu32_thumb_pc as [l_pc |] eqn: Hpc; [| inversion Hjit].
  remember jit_alu32_pre as l_pre eqn: Hpre_eq.
  remember (jit_alu32_thumb_store_v1 _) as l_st eqn: Hst_eq.
  remember (jit_alu32_thumb_reset_v1 _) as l_re eqn: Hre_eq.
  remember jit_alu32_post as l_post eqn: Hpost_eq.
  injection Hjit as Hl_eq Hn_eq.
  subst n1.

  assert (Heq: tp_len + List.length jit_alu32_pre < List.length tp_bin).
  {
    subst.
    rewrite List.app_length in HMAX.
    lia.
  }
  eapply jit_alu32_pre_v2_eq in Heq.
  destruct Heq as (tp_pre & Hpre & Hlist_pre & Hlen_pre & Hother_pre & Hl_pre).
  rewrite Hpre.
  eapply get_alu32_jit_list4_eq with
    (tp_len := tp_len + Datatypes.length jit_alu32_pre) (tp_bin := tp_pre) in Hget.
  2:{
    subst.
    do 2 rewrite List.app_length in HMAX.
    do 2 rewrite Nat.add_assoc in HMAX.
    lia.
  }
  destruct Hget as (tp_get & Hget & Hlist_get & Hlen_get & Hother_get & Hl_get).
  rewrite Hget.

  eapply jit_alu32_thumb_pc_v2_eq with
    (tp_len := tp_len + Datatypes.length jit_alu32_pre +
        Datatypes.length l1) (tp_bin := tp_get) in Hpc.
  2:{
    subst.
    do 3 rewrite List.app_length in HMAX.
    do 3 rewrite Nat.add_assoc in HMAX.
    lia.
  }
  destruct Hpc as (tp_pc & Hpc & Hlist_pc & Hlen_pc & Hother_pc & Hl_pc).
  rewrite Hpc.

  symmetry in Hst_eq.
  eapply jit_alu32_thumb_store_v2_eq with
    (tp_len := tp_len + Datatypes.length jit_alu32_pre + Datatypes.length l1 + Datatypes.length l_pc) (tp_bin := tp_pc) in Hst_eq.
  2:{
    subst.
    do 4 rewrite List.app_length in HMAX.
    do 4 rewrite Nat.add_assoc in HMAX.
    lia.
  }
  destruct Hst_eq as (tp_st & Hst & Hlist_st & Hlen_st & Hother_st & Hl_st).
  rewrite Hst.

  symmetry in Hre_eq.
  eapply jit_alu32_thumb_reset_v2_eq with
    (tp_len := tp_len + Datatypes.length jit_alu32_pre + Datatypes.length l1 + Datatypes.length l_pc + Datatypes.length l_st) (tp_bin := tp_st) in Hre_eq.
  2:{
    subst.
    do 5 rewrite List.app_length in HMAX.
    do 5 rewrite Nat.add_assoc in HMAX.
    lia.
  }
  destruct Hre_eq as (tp_re & Hre & Hlist_re & Hlen_re & Hother_re & Hl_re).
  rewrite Hre.

  symmetry in Hpost_eq.
  subst.
  rewrite ! List.app_length in HMAX.
  rewrite ! Nat.add_assoc in HMAX.
  rewrite Hlen_pre in HMAX.
  rewrite Hlen_get in HMAX.
  rewrite Hlen_pc in HMAX.
  rewrite Hlen_st in HMAX.
  rewrite Hlen_re in HMAX.
  eapply jit_alu32_post_v2_eq in HMAX as Hpost_eq.
  destruct Hpost_eq as (tp_post & Hpost & Hlist_post & Hlen_post & Hother_post & Hl_post).
  rewrite Hpost.

  exists tp_post.
  split.
  {
    f_equal.
    f_equal.
    f_equal.
    rewrite ! List.app_length.
    rewrite ! Nat.add_assoc.
    reflexivity.
  }

  split.
  {
    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hl_post; [| lia].
      eapply Hl_re; [| lia].
      eapply Hl_st; [| lia].
      eapply Hl_pc; [| lia].
      eapply Hl_get; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hl_post; [| lia].
      eapply Hl_re; [| lia].
      eapply Hl_st; [| lia].
      eapply Hl_pc; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hl_post; [| lia].
      eapply Hl_re; [| lia].
      eapply Hl_st; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hl_post; [| lia].
      eapply Hl_re; [| lia].
      assumption.
    }

    rewrite list_in_bin_code_concat_iff.
    split.
    {
      eapply Hl_post; [| lia].
      assumption.
    }
    assumption.
  }

  split.
  {
    rewrite Hlen_pre.
    rewrite Hlen_get.
    rewrite Hlen_pc.
    rewrite Hlen_st.
    rewrite Hlen_re.
    assumption.
  }

  split.
  {
    intros i Hle.
    erewrite Hother_pre; [ |lia].
    erewrite Hother_get; [ |lia].
    erewrite Hother_pc; [ |lia].
    erewrite Hother_st; [ |lia].
    erewrite Hother_re; [ |lia].
    erewrite Hother_post; [ |lia].
    reflexivity.
  }

  intros ln len_n HL Hle.
  eapply Hl_post; [| lia].
  eapply Hl_re; [| lia].
  eapply Hl_st; [| lia].
  eapply Hl_pc; [| lia].
  eapply Hl_get; [| lia].
  eapply Hl_pre; [| lia].
  assumption.
Qed.

Lemma compose_jit_list_v4_eq:
  forall fuel l pc c n tp_len tp_bin
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (Hjit: compose_jit_list_v3 c fuel pc = Some (l, n)),
      exists tp_bin1,
      compose_jit_list_v4 c fuel pc tp_len tp_bin =
        Some (tp_len + List.length l, tp_bin1, n) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  unfold compose_jit_list_v3, compose_jit_list_v4; intros.
  eapply compose_jit_list_aux_v4_eq in Hjit; eauto.
Qed.

Lemma whole_compiler_aux_v4_eq:
  forall fuel l pc c tp_len tp_bin tp_kv kv
    (HMAX: tp_len + List.length l < List.length tp_bin)
    (Hjit: whole_compiler_aux_v3 c fuel pc tp_len = Some (kv, l)),
      exists tp_bin1,
      whole_compiler_aux_v4 c fuel pc tp_kv tp_len tp_bin =
        Some (tp_kv ++ kv, tp_len + List.length l, tp_bin1) /\
        list_in_bin_code l tp_len tp_bin1 = true /\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true).
Proof.
  induction fuel; simpl; intros.
  {
    inversion Hjit; subst; clear Hjit.
    exists tp_bin.
    rewrite app_nil_r.
    simpl in *.
    rewrite Nat.add_0_r in *.
    repeat (split; [reflexivity |]).
    intros ln len_n HL Hle.
    assumption.
  }

  destruct (_ =? _).
  {
    inversion Hjit; subst; clear Hjit.
    exists tp_bin.
    rewrite app_nil_r.
    simpl in *.
    rewrite Nat.add_0_r in *.
    repeat (split; [reflexivity |]).
    intros ln len_n HL Hle.
    assumption.
  }

  destruct find_instr as [ins| ] eqn: Hfind; [| inversion Hjit].
  destruct ins; try (eapply IHfuel; eauto).
  - destruct compose_jit_list_v3 as [(bl3 & len3) |] eqn: Hcomp3; [| inversion Hjit].
    destruct (_ <? _); [| inversion Hjit].
    destruct whole_compiler_aux_v3 as [(kv3 & l3) |] eqn: Hwhole3; [| inversion Hjit].
    injection Hjit as Hkv_eq Hl_eq.
    eapply compose_jit_list_v4_eq with (tp_len := tp_len) (tp_bin := tp_bin) in Hcomp3.
    2:{
      subst l.
      rewrite List.app_length in HMAX.
      rewrite Nat.add_assoc in HMAX.
      lia.
    }
    destruct Hcomp3 as (tp3 & Hcomp3 & Hlist_in3 & Hlen3 & Hnth3 & HL3).
    rewrite Hcomp3.
    subst.

    eapply IHfuel with (tp_len := tp_len + Datatypes.length bl3) (tp_bin := tp3) in Hwhole3.
    2:{
      rewrite List.app_length in HMAX.
      rewrite Nat.add_assoc in HMAX.
      lia.
    }
    destruct Hwhole3 as (tp4 & Hwhole4 & Hlist_in4 & Hlen4 & Hnth4 & HL4).
    exists tp4.
    split.
    {
      rewrite Hwhole4.
      f_equal.
      f_equal.
      rewrite <- List.app_assoc.
      simpl.
      rewrite List.app_length.
      rewrite Nat.add_assoc.
      f_equal.
    }

    split.
    {
      rewrite list_in_bin_code_concat_iff.
      split; [| assumption].
      eapply HL4; eauto.
    }

    split.
    {
      rewrite Hlen3.
      assumption.
    }

    split.
    {
      intros i Hle.
      rewrite Hnth3; [| lia].
      apply Hnth4; auto.
      lia.
    }

    intros ln len_n Hlist Hle.
    eapply HL4; eauto.
    lia.

  - destruct find_instr as [ins1 |] eqn: Hfind1 in Hjit; [| inversion Hjit].
    rewrite Hfind1.
    destruct ins1; try (eapply IHfuel; eauto).

    destruct compose_jit_list_v3 as [(bl3 & len3) |] eqn: Hcomp3; [| inversion Hjit].
    destruct (_ <? _); [| inversion Hjit].
    destruct whole_compiler_aux_v3 as [(kv3 & l3) |] eqn: Hwhole3; [| inversion Hjit].
    injection Hjit as Hkv_eq Hl_eq.
    eapply compose_jit_list_v4_eq with (tp_len := tp_len) (tp_bin := tp_bin) in Hcomp3.
    2:{
      subst l.
      rewrite List.app_length in HMAX.
      rewrite Nat.add_assoc in HMAX.
      lia.
    }
    destruct Hcomp3 as (tp3 & Hcomp3 & Hlist_in3 & Hlen3 & Hnth3 & HL3).
    rewrite Hcomp3.
    subst.

    eapply IHfuel with (tp_len := tp_len + Datatypes.length bl3) (tp_bin := tp3) in Hwhole3.
    2:{
      rewrite List.app_length in HMAX.
      rewrite Nat.add_assoc in HMAX.
      lia.
    }
    destruct Hwhole3 as (tp4 & Hwhole4 & Hlist_in4 & Hlen4 & Hnth4 & HL4).
    exists tp4.
    split.
    {
      rewrite Hwhole4.
      f_equal.
      f_equal.
      rewrite <- List.app_assoc.
      simpl.
      rewrite List.app_length.
      rewrite Nat.add_assoc.
      f_equal.
    }

    split.
    {
      rewrite list_in_bin_code_concat_iff.
      split; [| assumption].
      eapply HL4; eauto.
    }

    split.
    {
      rewrite Hlen3.
      assumption.
    }

    split.
    {
      intros i Hle.
      rewrite Hnth3; [| lia].
      apply Hnth4; auto.
      lia.
    }

    intros ln len_n Hlist Hle.
    eapply HL4; eauto.
    lia.

  - destruct find_instr as [ins1 |] eqn: Hfind1 in Hjit; [| inversion Hjit].
    rewrite Hfind1.
    destruct ins1; try (eapply IHfuel; eauto).

    destruct compose_jit_list_v3 as [(bl3 & len3) |] eqn: Hcomp3; [| inversion Hjit].
    destruct (_ <? _); [| inversion Hjit].
    destruct whole_compiler_aux_v3 as [(kv3 & l3) |] eqn: Hwhole3; [| inversion Hjit].
    injection Hjit as Hkv_eq Hl_eq.
    eapply compose_jit_list_v4_eq with (tp_len := tp_len) (tp_bin := tp_bin) in Hcomp3.
    2:{
      subst l.
      rewrite List.app_length in HMAX.
      rewrite Nat.add_assoc in HMAX.
      lia.
    }
    destruct Hcomp3 as (tp3 & Hcomp3 & Hlist_in3 & Hlen3 & Hnth3 & HL3).
    rewrite Hcomp3.
    subst.

    eapply IHfuel with (tp_len := tp_len + Datatypes.length bl3) (tp_bin := tp3) in Hwhole3.
    2:{
      rewrite List.app_length in HMAX.
      rewrite Nat.add_assoc in HMAX.
      lia.
    }
    destruct Hwhole3 as (tp4 & Hwhole4 & Hlist_in4 & Hlen4 & Hnth4 & HL4).
    exists tp4.
    split.
    {
      rewrite Hwhole4.
      f_equal.
      f_equal.
      rewrite <- List.app_assoc.
      simpl.
      rewrite List.app_length.
      rewrite Nat.add_assoc.
      f_equal.
    }

    split.
    {
      rewrite list_in_bin_code_concat_iff.
      split; [| assumption].
      eapply HL4; eauto.
    }

    split.
    {
      rewrite Hlen3.
      assumption.
    }

    split.
    {
      intros i Hle.
      rewrite Hnth3; [| lia].
      apply Hnth4; auto.
      lia.
    }

    intros ln len_n Hlist Hle.
    eapply HL4; eauto.
    lia.
Qed.

Lemma whole_compiler_v4_eq:
  forall l c kv
    (HMAX: List.length l < JITTED_LIST_MAX_LENGTH)
    (Hjit: whole_compiler_v3 c = Some (kv, l)),
      exists tp_bin1,
      whole_compiler_v4 c =
        Some (kv, List.length l, tp_bin1) /\
        list_in_bin_code l 0 tp_bin1 = true (*/\
        List.length tp_bin = List.length tp_bin1 /\
        (forall i, i < tp_len -> List.nth_error tp_bin i = List.nth_error tp_bin1 i) /\
        (forall ln len_n, list_in_bin_code ln len_n tp_bin = true ->
          len_n + List.length ln <= tp_len ->
          list_in_bin_code ln len_n tp_bin1 = true) *).
Proof.
  unfold whole_compiler_v3, whole_compiler_v4; intros.
  eapply whole_compiler_aux_v4_eq with (tp_bin := List32.create_int_list JITTED_LIST_MAX_LENGTH) in Hjit.
  2:{
    simpl in *.
    unfold JITTED_LIST_MAX_LENGTH in HMAX.
    lia.
  }
  destruct Hjit as (tp1 & Hwhole & Hlist & Hlen & Hother & HL).
  exists tp1.
  rewrite Hwhole.
  split.
  - rewrite app_nil_l.
    simpl.
    f_equal.
  - assumption.
Qed.