From compcert.common Require Import Values Memory AST.
From compcert.lib Require Import Integers Maps.
From compcert.arm Require Import AsmSyntax BinSyntax BinDecode ABinSem.

From bpf.rbpf32 Require Import TSSyntax JITConfig.

From bpf.jit Require Import Arm32Reg ThumbInsOp.
From bpf.jit Require Import BitfieldLemma ThumbJITLtac.

From Coq Require Import List ZArith Lia.
Open Scope nat_scope.
Open Scope bool_scope.
Import ListNotations.

Lemma decode_str_i_13:
  forall br,
      decode_thumb
      (encode_arm32
         (encode_arm32 (int_of_breg br) (Int.mul (int_of_breg br) (Int.repr 4)) 12 4)
         (encode_arm32 (int_of_ireg IR13) STR_I_OP 0 4) 16 16) =
        Some (Pstr_a (ireg_of_breg br) IR13
                (SOimm (Int.mul (int_of_breg br) (Int.repr 4)))).
Proof.
  intros.
  unfold decode_thumb, STR_I_OP.

  atp_de.
  unfold decode_thumb2.
  atp_de.

  replace (_ && _) with true by (destruct br; simpl; auto).

  rewrite int2ireg_int_of_ireg_same.
  rewrite int2ireg_int_of_breg_same.
  simpl.

  repeat f_equal.
  destruct br; simpl; auto.
Qed.

Lemma decode_ldr_i_12:
  forall br,
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg br) (Int.mul (int_of_breg br) (Int.repr 4)) 12 4)
         (encode_arm32 (int_of_ireg IR12) LDR_I_OP 0 4) 16 16) =
        Some (Pldr (ireg_of_breg br) IR12
                (SOimm (Int.mul (int_of_breg br) (Int.repr 4)))).
Proof.
  intros.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  replace (_ && _) with true by (destruct br; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.
  rewrite int2ireg_int_of_breg_same.
  simpl.
  repeat f_equal.

  destruct br; simpl; auto.
Qed.

Lemma decode_ldr_i_12_11:
  decode_thumb
    (encode_arm32 (encode_arm32 (Int.repr 11) (Int.repr 44) 12 4)
        (encode_arm32 (int_of_ireg IR12) LDR_I_OP 0 4) 16 16) =
        Some (Pldr IR11 IR12 (SOimm (Int.repr 44))).
Proof.
  atp_de.
Qed.

Lemma decode_ldr_i_13:
  forall br,
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg br) (Int.mul (int_of_breg br) (Int.repr 4)) 12 4)
         (encode_arm32 (int_of_ireg IR13) LDR_I_OP 0 4) 16 16) =
        Some (Pldr_a (ireg_of_breg br) IR13
                (SOimm (Int.mul (int_of_breg br) (Int.repr 4)))).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  replace (_ && _) with true by (destruct br; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.
  rewrite int2ireg_int_of_breg_same.
  simpl.
  repeat f_equal.
  destruct br; simpl; auto.
Qed.

Lemma decode_ldr_i_13_11:
  decode_thumb
    (encode_arm32 (encode_arm32 (Int.repr 11) (Int.repr 44) 12 4)
        (encode_arm32 (int_of_ireg IR13) LDR_I_OP 0 4) 16 16) =
        Some (Pldr_a IR11 IR13 (SOimm (Int.repr 44))).
Proof.
  atp_de.
Qed.

Lemma decode_ldr_i_13_13:
  decode_thumb
      (encode_arm32 (encode_arm32 (int_of_ireg IR13) Int.zero 12 4)
         (encode_arm32 (int_of_ireg IR13) LDR_I_OP 0 4) 16 16) =
        Some (Pldr_a IR13 IR13 (SOimm Int.zero)).
Proof.
  atp_de.
Qed.

Lemma decode_str_i_12_11:
  decode_thumb
    (encode_arm32 (encode_arm32 (Int.repr 11) (Int.repr 44) 12 4)
      (encode_arm32 (int_of_ireg IR12) STR_I_OP 0 4) 16 16) =
      Some (Pstr IR11 IR12 (SOimm (Int.repr 44))).
Proof.
  atp_de.
Qed.

Lemma decode_str_i_13_11:
  decode_thumb
      (encode_arm32 (encode_arm32 (Int.repr 11) (Int.repr 44) 12 4)
         (encode_arm32 (int_of_ireg IR13) STR_I_OP 0 4) 16 16) =
      Some (Pstr_a IR11 IR13 (SOimm (Int.repr 44))).
Proof.
  atp_de.
Qed.

Lemma decode_str_i_12:
  forall br,
  decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg br) (Int.mul (int_of_breg br) (Int.repr 4)) 12 4)
         (encode_arm32 (int_of_ireg IR12) STR_I_OP 0 4) 16 16) =
        Some (Pstr (ireg_of_breg br) IR12
                (SOimm (Int.mul (int_of_breg br) (Int.repr 4)))).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  replace (_ && _) with true by (destruct br; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.
  rewrite int2ireg_int_of_breg_same.
  simpl.
  repeat f_equal.
  destruct br; simpl; auto.
Qed.