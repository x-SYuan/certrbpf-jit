From compcert.common Require Import Values Memory AST.
From compcert.lib Require Import Integers Maps.
From compcert.arm Require Import AsmSyntax BinSyntax BinDecode ABinSem.

From bpf.rbpf32 Require Import TSSyntax JITConfig.

From bpf.jit Require Import Arm32Reg ThumbInsOp.
From bpf.jit Require Import BitfieldLemma ThumbJITLtac.

From Coq Require Import List ZArith Lia.
Open Scope nat_scope.
Open Scope bool_scope.
Import ListNotations.

Lemma decode_add_r:
  forall dst src,
    decode_thumb
      (encode_arm32
         (encode_arm32 (if Int.lt (int_of_breg dst) (Int.repr 8) then Int.zero else Int.one)
            (encode_arm32 (int_of_ireg src)
               (encode_arm32
                  (if Int.lt (int_of_breg dst) (Int.repr 8)
                   then int_of_breg dst
                   else Int.sub (int_of_breg dst) (Int.repr 8)) ADD_R_OP 0 3) 3 4) 7 1) NOP_OP 16
         16) = Some (Padd (ireg_of_breg dst) (ireg_of_breg dst)
                (SOreg src)).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb1.
  atp_de.

  replace (Int.bitfield_insert _ _ _ _) with (int_of_breg dst) by (destruct dst; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.
  rewrite int2ireg_int_of_breg_same.
  f_equal.
Qed.

Lemma decode_sub_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) (int_of_ireg src) 8 4)
         (encode_arm32 (int_of_breg dst) SUB_R_OP 0 4) 16 16) =
      Some (Psub (ireg_of_breg dst) (ireg_of_breg dst)
                (SOreg src)).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  do 2 replace (Int.unsigned_bitfield_extract _ _ _) with Int.zero by (destruct src; simpl; auto).
  int_eq.
  simpl.
  rewrite int2ireg_int_of_breg_same.
  replace (Int.unsigned_bitfield_extract _ _ _) with (int_of_ireg src) by (destruct src; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.
  f_equal.
Qed.

Lemma decode_mul_r:
  forall dst src,
    decode_thumb
      (encode_arm32
         (encode_arm32 (Int.repr 15) (encode_arm32 (int_of_breg dst) (int_of_ireg src) 8 4) 12 4)
         (encode_arm32 (int_of_breg dst) MUL_OP 0 4) 16 16) =
      Some (Pmul (ireg_of_breg dst) (ireg_of_breg dst) src).
Proof.
  unfold decode_thumb.
  atp_de.
  unfold decode_thumb2.
  atp_de.

  rewrite int2ireg_int_of_breg_same.

  replace (Int.unsigned_bitfield_extract _ _ _) with (int_of_ireg src) by (destruct src; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.

  replace (Int.unsigned_bitfield_extract _ _ _) with Int.zero by (destruct src; simpl; auto).
  int_eq.
  f_equal.
Qed.

Lemma decode_orr_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) (int_of_ireg src) 8 4)
         (encode_arm32 (int_of_breg dst) ORR_R_OP 0 4) 16 16) =
      Some (Porr (ireg_of_breg dst) (ireg_of_breg dst)
                (SOreg src)).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with Int.zero by (destruct src; simpl; auto).
  int_eq. simpl.
  rewrite int2ireg_int_of_breg_same.

  replace (Int.unsigned_bitfield_extract _ _ _) with (int_of_ireg src) by (destruct src; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.
  f_equal.
Qed.

Lemma decode_and_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) (int_of_ireg src) 8 4)
         (encode_arm32 (int_of_breg dst) AND_R_OP 0 4) 16 16) =
      Some (Pand (ireg_of_breg dst) (ireg_of_breg dst)
                (SOreg src)).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with Int.zero by (destruct src; simpl; auto).
  int_eq. simpl.
  rewrite int2ireg_int_of_breg_same.

  replace (Int.unsigned_bitfield_extract _ _ _) with (int_of_ireg src) by (destruct src; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.
  f_equal.
Qed.

Lemma decode_xor_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_breg dst) (int_of_ireg src) 8 4)
         (encode_arm32 (int_of_breg dst) EOR_R_OP 0 4) 16 16) =
      Some (Peor (ireg_of_breg dst) (ireg_of_breg dst)
                (SOreg src)).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with Int.zero by (destruct src; simpl; auto).
  int_eq. simpl.
  rewrite int2ireg_int_of_breg_same.

  replace (Int.unsigned_bitfield_extract _ _ _) with (int_of_ireg src) by (destruct src; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.
  f_equal.
Qed.

Lemma decode_mov_r:
  forall dst src,
    decode_thumb
      (encode_arm32
         (encode_arm32 (if Int.lt (int_of_breg dst) (Int.repr 8) then Int.zero else Int.one)
            (encode_arm32 (int_of_ireg src)
               (encode_arm32
                  (if Int.lt (int_of_breg dst) (Int.repr 8)
                   then int_of_breg dst
                   else Int.sub (int_of_breg dst) (Int.repr 8)) MOV_R_OP 0 3) 3 4) 7 1) NOP_OP 16
         16) = Some (Pmov (ireg_of_breg dst) (SOreg src)).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb1.
  atp_de.
  replace (Int.bitfield_insert _ _ _ _) with (int_of_breg dst) by (destruct dst; simpl; auto).
  rewrite int2ireg_int_of_breg_same.
  rewrite int2ireg_int_of_ireg_same.
  f_equal.
Qed.

Lemma decode_div_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (int_of_ireg src) (encode_arm32 (int_of_breg dst) UDIV_HI_OP 8 4) 0 4)
         (encode_arm32 (int_of_breg dst) UDIV_LO_OP 0 4) 16 16) =
    Some (Pudiv (ireg_of_breg dst) (ireg_of_breg dst) src).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.
  rewrite int2ireg_int_of_breg_same.
  rewrite int2ireg_int_of_ireg_same.
  simpl.
  f_equal.
Qed.

Lemma decode_lsl_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (Int.repr 15) (encode_arm32 (int_of_breg dst) (int_of_ireg src) 8 4) 12 4)
         (encode_arm32 (int_of_breg dst) LSL_R_OP 0 4) 16 16) = Some (Plsl (ireg_of_breg dst) (ireg_of_breg dst) src).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  rewrite int2ireg_int_of_breg_same.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with (int_of_ireg src) by (destruct src; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with Int.zero by (destruct src; simpl; auto).
  int_eq.
  f_equal.
Qed.

Lemma decode_lsr_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (Int.repr 15) (encode_arm32 (int_of_breg dst) (int_of_ireg src) 8 4) 12 4)
         (encode_arm32 (int_of_breg dst) LSR_R_OP 0 4) 16 16) = Some (Plsr (ireg_of_breg dst) (ireg_of_breg dst) src).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  rewrite int2ireg_int_of_breg_same.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with (int_of_ireg src) by (destruct src; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with Int.zero by (destruct src; simpl; auto).
  int_eq.
  f_equal.
Qed.

Lemma decode_asr_r:
  forall dst src,
    decode_thumb
      (encode_arm32 (encode_arm32 (Int.repr 15) (encode_arm32 (int_of_breg dst) (int_of_ireg src) 8 4) 12 4)
         (encode_arm32 (int_of_breg dst) ASR_R_OP 0 4) 16 16) = Some (Pasr (ireg_of_breg dst) (ireg_of_breg dst) src).
Proof.
  unfold decode_thumb.
  atp_de.

  unfold decode_thumb2.
  atp_de.

  rewrite int2ireg_int_of_breg_same.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with (int_of_ireg src) by (destruct src; simpl; auto).
  rewrite int2ireg_int_of_ireg_same.

  repeat replace (Int.unsigned_bitfield_extract _ _ _) with Int.zero by (destruct src; simpl; auto).
  int_eq.
  f_equal.
Qed.

Lemma decode_mov_12_1:
  decode_thumb
    (encode_arm32
       (encode_arm32 Int.one
          (encode_arm32 Int.one (encode_arm32 (Int.repr 4) MOV_R_OP 0 3) 3 4) 7 1) NOP_OP 16 16) =
    Some (Pmov IR12 (SOreg IR1)).
Proof.
  atp_de.
Qed.

Lemma decode_bx:
  decode_thumb (encode_arm32 (encode_arm32 (int_of_ireg IR14) BX_OP 3 4) NOP_OP 16 16) =
  Some (Pbreg IR14).
Proof.
  atp_de.
Qed.