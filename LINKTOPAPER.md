This document explains how to locate in the Coq developments the theorems stated in the paper.

# Source of the development and paper

The current VM contains a clone of our gitlab repository (https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/tree/CAV24-AE)
which contains both the development and the current version of the [paper](/paper/CAV24-draft.pdf)

This is the version this [LINKTOPAPER.md] refers to.

In the following, we give more details about the structure of the development and refer to the appropriate statements in the paper. 

# Decode-Encode Consistency
Section 4, `Symbolic CompCert ARM Interpreter` says `Lemma 1 (Decode-Encode Consistency)`, we prove each jited related ARM instruction has a consistent decode-encode lemma:
- Addition with regiser, [ADD_R](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/ThumbDecodeEncodeALU.v?ref_type=heads#L326)
```coq
Lemma decode_add_r:
  forall dst src,
    decode_thumb
      (encode_arm32
         (encode_arm32 (if Int.lt (int_of_breg dst) (Int.repr 8) then Int.zero else Int.one)
            (encode_arm32 (int_of_ireg src)
               (encode_arm32
                  (if Int.lt (int_of_breg dst) (Int.repr 8)
                   then int_of_breg dst
                   else Int.sub (int_of_breg dst) (Int.repr 8)) ADD_R_OP 0 3) 3 4) 7 1) NOP_OP 16
         16) = Some (Padd (ireg_of_breg dst) (ireg_of_breg dst)
                (SOreg src)).

```
- Sub with register [SUB_R](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/ThumbDecodeEncodeALU.v?ref_type=heads#L561)
- ...

Readers can find all lemmas in [ThumbDecodeEncodeALU.v](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/ThumbDecodeEncodeALU.v), [ThumbDecodeEncodeALU0.v](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/ThumbDecodeEncodeALU0.v) and [ThumbDecodeEncodeMEM.v](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/ThumbDecodeEncodeMEM.v).

# CompCert-ARM-Binary

Section 4 also shows many definitions, readers can find them in the `arm/ABinSem.v` of `compcert-arm-bin.zip`.
- `init_state`: L786
```coq
Definition init_state  (s:signature) (sz:Z) (pos:ptrofs) (l:list val) (m:mem) : aoutcome :=
  match alloc_arguments l (sig_args s)  int_param_regs with
  | None => AStuck
  | Some l =>
      let rs := init_regset l regset0 in
      match allocframe sz pos m rs with
      | AStuck => AStuck
      | ANext rs' stk' stkb' m1' =>
          ANext (rs'# IR14 <- (Aval (Sval (Sreg PC) (Ptrofs.repr 2))) (**r may CompCertBin doesn't define operations like abstract(pc) +2 *)
                                #PC <- (rs # IR0)) stk' stkb' m1'
      end
  end.
```
- `is_final_state`: L805
```coq
Definition is_final_state (rs:aregset) :=
  (** The current PC is the value of the initial return address *)
  aval_eq (rs PC) (Aval (Sval (Sreg PC) (Ptrofs.repr 2))) (* This is the value of RA *)
  && (* The stack pointer is restored *)
    aval_eq (rs SP) (Aval (Rval (Sreg SP)))
  &&
    (** All the callee-save are restored *)
    List.forallb (fun r => aval_eq (rs (preg_of r)) (Aval (Rval (Sreg (preg_of r))))) (int_callee_save_regs ++ float_callee_save_regs).
```
- `bin_interp` and `bin exec`: L891
```coq
Fixpoint bin_interp (rt:rettype) (n:nat) (rs: aregset) (stk : astack) (stkb: block) (m : mem) : option (val*mem) :=
  if is_final_state rs
  then get_result rt (rs IR0) m
  else
    match n with
    | O => None (* Not enough fuel *)
    | Datatypes.S n =>
        match find_instr (rs # PC) m with
        | None => None (* Invalid decoding *)
        | Some (i,sz) =>
            match aexec_instr sz i rs stk stkb m with
            | ANext rs' stk' stkb m' => bin_interp rt n rs' stk' stkb m'
            | AStuck => None
            end
        end
    end.

Definition bin_exec (n:nat) (s:signature) (sz:Z) (pos:ptrofs)  (args: list val) (m : mem) : option (val*mem) :=
  match init_state s sz pos args m with
  | ANext rs stk stkb m1 => bin_interp (sig_res s) n rs stk stkb m1
  | AStuck => None
  end.
```
# 


In the section 5`A verified Just-In-Time Compiler for rBPF`:
- Page 13 `The rBPF state is a pair state ::= (R, M )`: the [corresponding coq code](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/TSSyntax.v?ref_type=heads#L450)
```coq
Inductive state: Type :=
  | State: regset -> mem -> state.
```
- Page 13 `Transition Semantics of rBPF`: the [coq code](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/TSSemantics.v?ref_type=heads#L39)
```coq
Inductive step (c: code) (ge: genv): state -> trace -> state -> Prop :=
  | exec_step_alu32:
   ...
```
- Page 14 `Analyzer` Coq implementation: [coq code](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/Analyzer.v?ref_type=heads#L90)
```coq
Definition analyzer (c: code): option (list (nat * bpf_code)) :=
  analyzer_aux c (List.length c) 0%nat.
```

- Page 14 `Transition Semantics of the Analyzer`: [coq code](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/TSSemanticsA.v?ref_type=heads#L39)
```coq
Inductive astep (c: code) (kl: list (nat * bpf_code)) (ge: genv): state -> trace -> state -> Prop :=
  | aexec_step_alu32:
   ...
```
- Page 14 `Lemma 2 (TS_A simulates TS_rBPF in one step)` : the [coq file](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/TSSemanticsAproof.v?ref_type=heads#L85)
```coq
Theorem astep_backward_simulation:
  forall ge bpf_get_call mem_map c kl rs0 m0 t rs1 m1,
    astep bpf_get_call mem_map c kl ge (State rs0 m0) t (State rs1 m1) ->
    List.length c <= MAX_BPF_LIST_INPUT ->
    analyzer c = Some kl ->
      star (step bpf_get_call mem_map c) ge (State rs0 m0) t (State rs1 m1).

```
- Page 14 `JITCompiler` Coq implementation: [the code](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/WholeCompiler.v#L42)
```coq
Definition whole_compiler (c: code): option (list (nat * nat) * bin_code) :=
  match analyzer c with
  | None => None
  | Some kl =>
    match kl with
    | [] => Some ([], []) (**r if rbpf doesn't have any alu32, returns [], or should be None? *)
    | _ =>
      match combiner kl with
      | None => None
      | Some bl => Some (concat_bin bl)
      end
    end
  end.
```

- Page 14 `Transition Semantics of HAVM`: [the code](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/TSSemanticsB.v?ref_type=heads#L67) 
```coq
Inductive bstep (c: code) (bl: list (nat *(nat * bin_code))) (jit_blk: block) (ge: genv):
    state -> trace -> state -> Prop :=
  | bexec_step_alu32:
   ...
```

- Page 14 `Lemma 3 (T SA simulates T SHAVM in one step)`: the [coq proof](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/TSSemanticsBproof.v?ref_type=heads#L939)
```coq
Theorem bstep_forward_simulation:
  forall ge bpf_get_call mem_map c bl jit_blk rs0 m0 t rs1 m1 kl l kv,
    astep bpf_get_call mem_map c kl ge (State rs0 m0) t (State rs1 m1) ->
    reg_inv (State rs0 m0) ->
    List.length c <= MAX_BPF_LIST_INPUT ->
    (forall ep l1, In (ep, l1) kl -> List.length l1 <= MAX_BPF_LIST_INPUT) ->
    valid_block_state (State rs0 m0) jit_blk ->
    combiner kl = Some bl ->
    concat_bin bl = (kv, l) ->
    code_subset_blk l 0 jit_blk (State rs0 m0) = true ->
      star (bstep bpf_get_call mem_map c bl jit_blk) ge (State rs0 m0) t (State rs1 m1).
   ...
```
# JIT Refinement

- Page 15 `Removing Intermediate Representation`: [the optmiziation code](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/WholeCompiler2.v?ref_type=heads#L122) (*NB: we did other optimization in WholeCompiler0.v and WholeCompiler1.v but didn't mention in the paper*)
```coq
Fixpoint get_alu32_jit_list (c: code) (fuel pc counter: nat) (ld_set st_set: listset):
  option (bin_code * listset * listset * nat) :=
...

Definition whole_compiler_v2 (c: code):
    option (list (nat * nat) * bin_code) :=
  whole_compiler_aux_v2 c (List.length c) 0%nat 0%nat.
```

- Page 15 `Refining Data Structure`: the `Record regSet` is [listset_regmap](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/ListSetRefinement.v?ref_type=heads#L7) in Coq.
```coq
Record listset_regmap: Type := {
  r0_b  : bool;
  r1_b  : bool;
  r2_b  : bool;
  r3_b  : bool;
  r4_b  : bool;
  r5_b  : bool;
  r6_b  : bool;
  r7_b  : bool;
  r8_b  : bool;
  r9_b  : bool;
  r10_b : bool;
}.
```

- Page `dx Refinement`: the `Record jit state` is [jit_state](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/jitcompiler/JITState.v?ref_type=heads#L30) in Coq.
```coq
Record jit_state := {
  input_len       : nat;
  input_ins       : List64AsArray.t;(**r rBPF binary instructions *)

  tp_kv           : ListKeyV.t; (**r the length of kv2 = the jit_ins_len *)
  use_IR11        : bool; (**r will use IR11 *)

  ld_set          : listset_regmap;
  st_set          : listset_regmap;
(*
  offset          : nat;  (**r the current offset of jitted code *) *)
  tp_bin_len      : nat;  (**r the number of all jitted instructions *)
  tp_bin          : List32.t;

  jit_mem         : mem;
}.
```

- Page 15 `Lemma 4 (∂x-Refinement Correctness)`: it consists of a set of lemmas for step-wise refinement proof from `whole_compiler_v_i` to `whole_compiler_v_i+1`:
```coq
(**r see jit/WholeCompile0.v *)
Theorem whole_compiler_v0_same:
  forall c kv l,
    whole_compiler_unfold c = Some (kv, l) ->
    whole_compiler_v0 c = Some (kv, l).
    
(**r see jit/WholeCompile1.v *)
Theorem whole_compiler_v1_same:
  forall c kv l,
    whole_compiler_v0 c = Some (kv, l) ->
    whole_compiler_v1 c = Some (kv, l).
...
```

# HAVM Virtual Machine

In the section 6 `HAVM: a hybrid Interpreter for rBPF`:
- Page 16 `jit call`: see the [coq file](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/BStateMonadOp.v?ref_type=heads#L77)
```coq
Definition jit_call_simplb: M unit := fun st =>
  match jit_call_simplb (tp_kv st) (regs_st st) (bpf_m st) with
  | None => None
  | Some (rs, m) => Some (tt, BState.upd_mem m (BState.upd_regs rs st))
  end.
```
- Page 16 `hybrid_step`: see the [coq file](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/BSemanticsMonadic.v?ref_type=heads#L104) 
```coq
Definition step : M unit :=
  do ins64<- eval_ins;
  do ins  <- decodeM ins64;
    match ins with
    ...
    
(**r the monadic semantics of our HAVM *)    
Definition bpf_interpreter (fuel: nat) (ctx_ptr: val): M val :=
  do _        <- upd_reg R1 ctx_ptr;
  do _        <- bpf_interpreter_aux fuel;
  do f        <- eval_flag;
    if flag_eq f BPF_SUCC_RETURN then
      do res  <- eval_reg R0;
        returnM res
    else
      returnM Vzero.

```
- Page 16 `Lemma 5 (Interpreter Refinement)`: we reuse the simplification proof technique from [SETTA23](https://gitlab.inria.fr/x-SYuan/certrbpfopt) by introducing a non-moandic model, therefore we have two lemmas: the equivalence proof between [our monadic one](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/BSemanticsMonadic.v) and this [non-monadic one](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/rbpf32/BSemanticsSimpl.v?ref_type=heads#L225), and the refinement between the non-monadic one and the [Transitive semantics](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/TSSemanticsB.v?ref_type=heads#L67) of our VM. We reused the result of SETTA23, i.e. the first lemma. The second refinement proof is in Coq [coq proof](https://gitlab.inria.fr/x-SYuan/certrbpf-jit/-/blob/CAV24-AE/jit/TSSemanticsBequiv.v?ref_type=heads#L554).
```coq
Theorem forward_from_ts_to_interpreter_simpl:
  forall rs1 rs2 st1 st2 m1 m2 c kl bl kv l ge jit_blk t mrs sl,
    bstep _bpf_get_call (memory_region_mapping mrs) c bl jit_blk ge
      st1 t st2 ->
      
      st1 = State rs1 m1 ->
      st2 = State rs2 m2 ->

    ...
    step c sl (List.length c) rs1 (List.length mrs) mrs m1 =
      Some (rs2, m2, BPF_OK).
```
