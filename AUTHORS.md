# Authors

- [Shenghao Yuan](https://shenghaoyuan.github.io/)
- [Frédéric Besson](http://people.rennes.inria.fr/Frederic.Besson/)
- [Jean-Pierre Talpin](http://www.irisa.fr/prive/talpin/)
