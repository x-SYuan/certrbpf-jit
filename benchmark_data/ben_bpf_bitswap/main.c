#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include "embUnit.h"
#include "timex.h"
#include "ztimer.h"

#if BPF_COQ == 0
#include "bpf.h"
#elif BPF_COQ == 1
#include "interpreter.h"
#else
#include "havm_interpreter.h"
#endif



const unsigned char bpf_input_bin[] = {
  0x71, 0x13, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xb4, 0x02, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
  0xb4, 0x04, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
  0x6c, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x71, 0x15, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x6c, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xbc, 0x26, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x4c, 0x46, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xa4, 0x06, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
  0x71, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xbc, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x5c, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x5c, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x7c, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x6c, 0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x4c, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x5c, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x7c, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x6c, 0x32, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x4c, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x54, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00,
  0x95, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static uint8_t _bpf_stack[512];

struct bpf_input_ctx {
  uint8_t value;
  uint8_t bit1;
  uint8_t bit2;
};

struct bpf_input_ctx ctx = {
  .value = 156,
  .bit1 = 3,
  .bit2 = 6,
};

#if BPF_COQ == 1 || BPF_COQ == 2
static struct memory_region mr_stack = {.start_addr = (uintptr_t)_bpf_stack,
                                        .block_size = sizeof(_bpf_stack),
                                        .block_perm = Freeable,
                                        .block_ptr = _bpf_stack};
                                        
static struct memory_region mr_ctx = {.start_addr = (uintptr_t) &ctx,
                                        .block_size = sizeof(ctx),
                                        .block_perm = Readable,
                                        .block_ptr = (unsigned char *) (uintptr_t) &ctx};
#endif
        
#if BPF_COQ == 2

__attribute((aligned(4))) unsigned int tp_bin_list[JITTED_LIST_MAX_LENGTH];
struct key_value2 tp_kv_list[sizeof(bpf_input_bin)/8];

__attribute__ ((noinline)) void _magic_function(unsigned int ofs, struct havm_state* st){
  int res = 0;
  __asm volatile (
    "orr %[input_0], #0x1\n\t"
    "mov r12, sp\n\t"
    "sub sp, sp, #48\n\t"
    "str r12, [sp, #0]\n\t"
    "mov pc, %[input_0]\n\t"
    : [result] "=r" (res)
    : [input_1] "r" (st), [input_0] "r" (tp_bin_list + ofs)
    : "cc" //The instruction modifies the condition code flags
  );
  return ;
}
#endif



int main(void){  
  float duration = 0;
  for (int loop_size = 0; loop_size < 1000; loop_size++) {
  
#if BPF_COQ == 0
  bpf_t bpf = {
    .application = (uint8_t*)&bpf_input_bin,
    .application_len = sizeof(bpf_input_bin),
    .stack = _bpf_stack,
    .stack_size = sizeof(_bpf_stack),
    .flags = BPF_FLAG_PREFLIGHT_DONE,
  };
  bpf_setup(&bpf);
  int64_t res = 0;
  
#elif BPF_COQ == 1
  struct memory_region memory_regions[] = { mr_stack, mr_ctx };
  struct bpf_state st = {
    .state_pc = 0,
    .regsmap = {0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, (uintptr_t)_bpf_stack+512},
    .bpf_flag = vBPF_OK,
    .mrs = memory_regions,
    .mrs_num = ARRAY_SIZE(memory_regions),
    .ins = (unsigned long long *) bpf_input_bin,
    .ins_len = sizeof(bpf_input_bin)/8,
  };
  
#else
  struct memory_region memory_regions[] = { mr_stack, mr_ctx };
  
  struct jit_state jst = {
    .input_len = sizeof(bpf_input_bin)/8,
    .input_ins = (unsigned long long *) bpf_input_bin,
    .tp_kv = tp_kv_list,
    .use_IR11 = 0,
    .ld_set = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U},
    .st_set = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U},
    .tp_bin_len = 0,
    .tp_bin = tp_bin_list,
  };
  
  struct havm_state hst = {
    .regsmap = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, (uintptr_t)_bpf_stack+512},
    .pc_loc = 0,
    .bpf_flag = vBPF_OK,
    .mrs_num = ARRAY_SIZE(memory_regions),
    .mrs = memory_regions,
    .input_len = sizeof(bpf_input_bin)/8,
    .input_ins = (unsigned long long *) bpf_input_bin,
    .tp_kv = tp_kv_list,
    .tp_bin_len = 0,
    .tp_bin = tp_bin_list,
  };
  
  
  whole_compiler(&jst);
#endif

  uint32_t begin = ztimer_now(ZTIMER_USEC); // unsigned long long -> uint64_t
#if BPF_COQ == 0
  int result = bpf_execute_ctx(&bpf, &ctx, sizeof(ctx), &res);
  
  //printf("Vanilla-rBPF C result = 0x:%x\n", (unsigned int)res);
  
#elif BPF_COQ == 1
  int result = bpf_interpreter(&st, 10000, (uintptr_t) &ctx);
  
  //printf("flag=%d\n", st.bpf_flag);
  //printf("CertrBPF C result = 0x:%x\n", (unsigned int)result); //= 0xd4
  
#else
  int result = havm_interpreter(&hst, 10000, (uintptr_t) &ctx);
  
  //printf("flag=%d\n", hst.bpf_flag);
  //printf("CertrBPF-JIT C result = 0x:%x\n", (unsigned int)result);
  //_magic_function(0, &hst);
  //printf("CertrBPF-JIT-Pure C result = 0x:%x\n", (unsigned int)(hst.regsmap[0]));
#endif

  uint32_t end = ztimer_now(ZTIMER_USEC);
  duration = (float)(end-begin) + duration;
  }
  printf("execution time:%f\n", duration);
  return 0;
}
