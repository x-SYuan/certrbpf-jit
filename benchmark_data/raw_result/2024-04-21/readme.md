# Evaluation Data

The raw result of each benchmark can be found in the corresponding files:
- `XXX_terminal_b.log_txt` records the input shell commands of the current benchmark
- `XXX_terminal_a.log_txt` records the output execution result (unit is _us_)

For each benchmark, we execute them on three VMs, we execute three times for one benchmark on one VM for average value. Some benchmarks has a 1000 times loop within the benchmark for precision, for example `incr` has only three instructions and the single exeuction is too small to measure the result correctly. 

|Interpreter | incr | square | bitswap | fib  | sock_buf | memcpy | fletcher32 | bsort |
|------------|------|--------|---------|------|----------|--------|------------|-------|
|vanilla-rBPF|8.438 |8.500   |42.252   |94.377|325       |887     |2283        |11722  |
|CertrBPF    |5.127 |5.501   |34.751   |92.377|300       |822     |1980        |10697  |
|HAVM        |4.375 |4.374   |15.625   |51.750|185       |588     |1196        |7120   |
