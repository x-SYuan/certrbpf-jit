#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include "embUnit.h"
#include "timex.h"
#include "ztimer.h"

#if BPF_COQ == 0
#include "bpf.h"
#elif BPF_COQ == 1
#include "interpreter.h"
#else
#include "havm_interpreter.h"
#endif


unsigned char bpf_input_bin[] = /* compiled by CompCertBPF */{
  0xbc, 0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x14, 0x0a, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00,
  0x63, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x63, 0x9a, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x63, 0x6a, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x61, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x61, 0x15, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xb4, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xbc, 0x23, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x04, 0x03, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
  0x7d, 0x31, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xb4, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xbc, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x1c, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x04, 0x04, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
  0x7d, 0x43, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xb4, 0x04, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
  0xbc, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x6c, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xbc, 0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x0c, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x61, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x61, 0x46, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xdd, 0x60, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x63, 0x64, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x63, 0x04, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x04, 0x03, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
  0x05, 0x00, 0xf0, 0xff, 0x00, 0x00, 0x00, 0x00,
  0x04, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
  0x05, 0x00, 0xea, 0xff, 0x00, 0x00, 0x00, 0x00,
  0x61, 0xa6, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x61, 0xa9, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x04, 0x0a, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00,
  0x95, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
}; 

int unsort_list[] = {5923, 3314, 6281, 2408, 9997, 4393, 772, 3983, 4083, 3212, 9096, 1973, 7792, 1627, 1812, 1683, 4615, 8370, 7379, 1188, 2511, 1115, 9226, 9025, 1898, 5529, 3674, 7868, 750, 2393, 9372, 4370};

void print_sorted_list (int arr[], int size) {
  for (int i = 0; i < size; i++) {
    if (i%10 == 0) { printf("\n"); }
    printf("%04d, ", arr[i]);
  }
  return ;
}

struct test_md
{
    int size;
    int* arr; //__bpf_shared_ptr(int*, arr); //CompCert compiles int* as 32-bit
};

struct test_md bpf_input_ctx = {
  .size = (sizeof(unsort_list)/sizeof(unsort_list[0])),
  .arr = (intptr_t)unsort_list,
};
static uint8_t _bpf_stack[512];

#if BPF_COQ == 1 || BPF_COQ == 2
static struct memory_region mr_stack = {.start_addr = (uintptr_t)_bpf_stack,
                                        .block_size = sizeof(_bpf_stack),
                                        .block_perm = Freeable,
                                        .block_ptr = _bpf_stack};
                                        
static struct memory_region mr_arr = {.start_addr = (uintptr_t)unsort_list,
                                        .block_size = sizeof(unsort_list),
                                        .block_perm = Freeable,
                                        .block_ptr = unsort_list};
                                        
static struct memory_region mr_ctx = {.start_addr = (uintptr_t) &bpf_input_ctx,
                                        .block_size = sizeof(bpf_input_ctx),
                                        .block_perm = Readable,
                                        .block_ptr = (unsigned char *) (uintptr_t) &bpf_input_ctx};
#endif
        
#if BPF_COQ == 2

__attribute((aligned(4))) unsigned int tp_bin_list[JITTED_LIST_MAX_LENGTH];
struct key_value2 tp_kv_list[sizeof(bpf_input_bin)/8];

__attribute__ ((noinline)) void _magic_function(unsigned int ofs, struct havm_state* st){
  int res = 0;
  __asm volatile (
    "orr %[input_0], #0x1\n\t"
    "mov r12, sp\n\t"
    "sub sp, sp, #48\n\t"
    "str r12, [sp, #0]\n\t"
    "mov pc, %[input_0]\n\t"
    : [result] "=r" (res)
    : [input_1] "r" (st), [input_0] "r" (tp_bin_list + ofs)
    : "cc" //The instruction modifies the condition code flags
  );
  return ;
}
#endif



int main(void){

#if BPF_COQ == 0
  bpf_t bpf = {
    .application = (uint8_t*)&bpf_input_bin,
    .application_len = sizeof(bpf_input_bin),
    .stack = _bpf_stack,
    .stack_size = sizeof(_bpf_stack),
    .flags = BPF_FLAG_PREFLIGHT_DONE,
  };
  bpf_mem_region_t region;
  bpf_setup(&bpf);
  int64_t res = 0; 
  bpf_add_region(&bpf, &region,
                 (void*)unsort_list, sizeof(unsort_list), BPF_MEM_REGION_WRITE);
  
#elif BPF_COQ == 1
  struct memory_region memory_regions[] = { mr_stack, mr_arr, mr_ctx };
  struct bpf_state st = {
    .state_pc = 0,
    .regsmap = {0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, 0LLU, (uintptr_t)_bpf_stack+512},
    .bpf_flag = vBPF_OK,
    .mrs = memory_regions,
    .mrs_num = ARRAY_SIZE(memory_regions),
    .ins = (unsigned long long *) bpf_input_bin,
    .ins_len = sizeof(bpf_input_bin)/8,
  }; 
  
#else
  struct memory_region memory_regions[] = { mr_stack, mr_arr, mr_ctx };
  
  struct jit_state jst = {
    .input_len = sizeof(bpf_input_bin)/8,
    .input_ins = (unsigned long long *) bpf_input_bin,
    .tp_kv = tp_kv_list,
    .use_IR11 = 0,
    .ld_set = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U},
    .st_set = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U},
    .tp_bin_len = 0,
    .tp_bin = tp_bin_list,
  };
  
  struct havm_state hst = {
    .regsmap = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, (uintptr_t)_bpf_stack+512},
    .pc_loc = 0,
    .bpf_flag = vBPF_OK,
    .mrs_num = ARRAY_SIZE(memory_regions),
    .mrs = memory_regions,
    .input_len = sizeof(bpf_input_bin)/8,
    .input_ins = (unsigned long long *) bpf_input_bin,
    .tp_kv = tp_kv_list,
    .tp_bin_len = 0,
    .tp_bin = tp_bin_list,
  };
  
  
  whole_compiler(&jst);
#endif

  uint32_t begin = ztimer_now(ZTIMER_USEC); // unsigned long long -> uint64_t
#if BPF_COQ == 0
  int result = bpf_execute_ctx(&bpf, &bpf_input_ctx, sizeof(bpf_input_ctx), &res);
  
  //printf("Vanilla-rBPF C result = 0x:%x\n", (unsigned int)res);
  
#elif BPF_COQ == 1
  int result = bpf_interpreter(&st, 10000, (uintptr_t) &bpf_input_ctx);
  
  //printf("flag=%d\n", st.bpf_flag);
  //printf("CertrBPF C result = 0x:%x\n", (unsigned int)result); //= 0xd4
  
#else
  int result = havm_interpreter(&hst, 10000, (uintptr_t) &bpf_input_ctx);
  
  //printf("flag=%d\n", hst.bpf_flag);
  //printf("CertrBPF-JIT C result = 0x:%x\n", (unsigned int)result);
  //_magic_function(0, &ibpf_state.st);
  //printf("CertrBPF-JIT-Pure C result = 0x:%x\n", (unsigned int)(hst.regsmap[0]));
#endif

  uint32_t end = ztimer_now(ZTIMER_USEC);
  float duration = (float)(end-begin);
  
  printf("execution time:%f\n", duration);
  
  //print_sorted_list(unsort_list, (sizeof(unsort_list)/sizeof(unsort_list[0])));
  /*
  	0750, 0772, 1115, 1188, 1627, 1683, 1812, 1898, 1973, 2393, 
	2408, 2511, 3212, 3314, 3674, 3983, 4083, 4370, 4393, 4615, 
	5529, 5923, 6281, 7379, 7792, 7868, 8370, 9025, 9096, 9226
  */
  //printf("\n hello \n");
  return 0;
}
