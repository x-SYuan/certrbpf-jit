(**************************************************************************)
(*  This file is part of CertrBPF,                                        *)
(*  a formally verified rBPF verifier + interpreter + JIT in Coq.         *)
(*                                                                        *)
(*  Copyright (C) 2022 Inria                                              *)
(*                                                                        *)
(*  This program is free software; you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation; either version 2 of the License, or     *)
(*  (at your option) any later version.                                   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(**************************************************************************)

From Coq Require Import List ZArith Lia.
Import ListNotations.

From compcert.lib Require Import Integers.

(** This module presents a generic List as a fix-sized array in C *)

Module List64AsArray.

  Definition t := list int64.
  Definition index (l: t) (idx: nat): option int64 := 
    List.nth_error l idx.

  Fixpoint assign' (l: t) (cur: nat) (v: int64): option t :=
    match l with
    | [] => None (**r it should be impossible *)
    | hd :: tl =>
      match cur with
      | O => Some (v :: tl)
      | S n =>
        match assign' tl n v with
        | Some nl => Some (hd :: nl)
        | None => None
        end
      end
    end.

  Definition assign (l: t) (cur: nat) (v: int64): t :=
    match assign' l cur v with
    | Some nl => nl
    | None    => []
    end.

End List64AsArray.

Module List16.

  Definition t := list int.
  Definition index (l: t) (idx: int): option int :=
    List.nth_error l (Z.to_nat (Int.unsigned idx)).

  Fixpoint assign' (l: t) (cur: nat) (v: int): option t :=
    match l with
    | [] => None (**r it should be impossible *)
    | hd :: tl =>
      match cur with
      | O => Some (v :: tl)
      | S n =>
        match assign' tl n v with
        | Some nl => Some (hd :: nl)
        | None => None
        end
      end
    end.

  Definition assign (l: t) (cur: nat) (v: int): t :=
    match assign' l cur v with
    | Some nl => nl
    | None    => []
    end.

  Fixpoint create_int_list (l: nat): List16.t :=
    match l with
    | O => []
    | S n => Int.zero :: create_int_list n
    end.

End List16.

Module List32.

  Definition t := list int.
  Definition index (l: t) (idx: int): option int :=
    List.nth_error l (Z.to_nat (Int.unsigned idx)).

  Fixpoint assign' (l: t) (cur: nat) (v: int): option t :=
    match l with
    | [] => None (**r it should be impossible *)
    | hd :: tl =>
      match cur with
      | O => Some (v :: tl)
      | S n =>
        match assign' tl n v with
        | Some nl => Some (hd :: nl)
        | None => None
        end
      end
    end.

  Definition assign (l: t) (cur: nat) (v: int): t :=
    match assign' l cur v with
    | Some nl => nl
    | None    => []
    end.

  Fixpoint create_int_list (l: nat): List32.t :=
    match l with
    | O => []
    | S n => Int.zero :: create_int_list n
    end.

End List32.


Module ListNat.

  Definition t := list nat.
  Definition index (l: t) (idx: nat): option nat :=  List.nth_error l idx.

  Fixpoint assign (l: t) (cur v: nat): option t :=
    match l with
    | [] => None (**r it should be impossible *)
    | hd :: tl =>
      match cur with
      | O => Some (v :: tl)
      | S n =>
        match assign tl n v with
        | Some nl => Some (hd :: nl)
        | None => None
        end
      end
    end. (*

  Definition assign (l: t) (cur v: nat): t :=
    match assign' l cur v with
    | Some nl => nl
    | None    => []
    end. *)

  Fixpoint is_exists (l: t) (cur v: nat): bool :=
    match l with
    | [] => false
    | hd :: tl =>
      match cur with
      | O => false
      | S n =>
        if Nat.eqb hd v then
          true
        else
          is_exists tl n v
      end
    end.

  Fixpoint create_int_list (l: nat): ListNat.t :=
    match l with
    | O => []
    | S n => 0 :: create_int_list n
    end.

(** test
  Compute (is_exists [1;2;3;4;5] 4 5).
  Compute (is_exists [1;2;3;4;5] 4 4). *)

End ListNat.

Lemma List_assign_swap:
  forall l (ep ofs k_a v_a: nat) l1 l2
  (Hnodup : ep <> k_a)
  (HA0 : ListNat.assign l ep ofs = Some l1)
  (HA1 : ListNat.assign l1 k_a v_a = Some l2),
  exists l3,
    ListNat.assign l k_a v_a = Some l3 /\
    ListNat.assign l3 ep ofs = Some l2.
Proof.
  induction l; simpl; intros.
  { inversion HA0. }

  destruct ep.
  { destruct k_a.
    exfalso; apply Hnodup; reflexivity.

    inversion HA0; subst; clear HA0.
    simpl in HA1.
    destruct ListNat.assign as [nl |] eqn: Ha; inversion HA1; subst; clear HA1.
    exists (a :: nl); split; [reflexivity |].
    simpl.
    reflexivity.
  }

  destruct ListNat.assign as [nl |] eqn: Ha; inversion HA0; subst; clear HA0.
  simpl in HA1.
  destruct k_a.
  { inversion HA1; subst; clear HA1.
    exists (v_a :: l); split; [reflexivity |].
    simpl.
    rewrite Ha.
    reflexivity.
  }

  destruct ListNat.assign as [nl1 |] eqn: Ha1 in HA1; inversion HA1; subst; clear HA1.
  eapply IHl with (ep := ep) (k_a := k_a) in Ha; eauto.
  destruct Ha as (l3 & Ha3 & Ha4).
  rewrite Ha3.
  exists (a :: l3); split; [reflexivity |].
  simpl.
  rewrite Ha4.
  reflexivity.
Qed.

Lemma List_assign_index_same:
  forall l1 l2 v ofs,
  ListNat.assign l1 v ofs = Some l2 ->
    ListNat.index l2 v = Some ofs.
Proof.
  induction l1; simpl; intros.
  { inversion H. }

  destruct v.
  - inversion H; subst.
    simpl; auto.
  - destruct ListNat.assign as [nl |] eqn: Ha; inversion H; subst; clear H.
    simpl.
    eapply IHl1; eauto.
Qed.


Fixpoint list_iter {A B:Type} (l: list A) (b: B) (f: A -> B -> B): B :=
  match l with
  | [] => b
  | hd :: tl => list_iter tl (f hd b) f
  end.

Fixpoint list_iter2_aux {A B:Type} (fuel pc: nat) (default: A) (l: list A) (b: B) (f: A -> B -> B): B :=
  match fuel with
  | O => b
  | S n =>
    let hd := nth_default default l pc in
      list_iter2_aux n (S pc) default l (f hd b) f
  end.

Definition list_iter2 {A B:Type} (default: A) (l: list A) (b: B) (f: A -> B -> B): B :=
  list_iter2_aux (length l) 0 default l b f.

Lemma list_app:
  forall {A: Type} (l: list A) (a: A) (l1: list A),
  l1 ++ a :: l = (l1 ++ [a]) ++ l.
Proof.
  intros.
  rewrite <- app_assoc.
  simpl.
  reflexivity.
Qed.

Lemma list_nth_default:
  forall {A:Type} (default: A) l1 a l,
    nth_default default ((l1 ++ [a]) ++ l) (length l1) = a.
Proof.
  unfold nth_default; induction l1; simpl; intros.
  - reflexivity.
  - rewrite IHl1.
    reflexivity.
Qed.

Lemma list_iter2_aux_equiv:
  forall (A B:Type) (default: A) (l: list A) (a: A)(b: B) (f: A -> B -> B) (l1: list A) (l2: list A),
    list_iter2_aux (length l) (length l1) default (l1 ++ l) (f a b) f =
    list_iter2_aux (length l) (length l2) default (l2 ++ l) (f a b) f.
Proof.
  induction l; simpl; intros.
  - reflexivity.
  - rewrite list_app.
    rewrite list_app with (l3 := l2).
    rewrite ! list_nth_default.
    specialize (IHl a (f a0 b) f).
    specialize (IHl (l1 ++ [a]) (l2 ++ [a])).
    rewrite ! last_length in IHl.
    assumption.
Qed.

Lemma list_iter_equiv:
  forall (A B:Type) (default: A) (l: list A) (b: B) (f: A -> B -> B),
    list_iter2 default l b f = list_iter l b f.
Proof.
  unfold list_iter2.
  induction l; simpl; intros.
  - reflexivity.
  - rewrite <- IHl.
    unfold nth_default.
    simpl.
    assert (Heq: list_iter2_aux (length l) 1 default (a :: l) (f a b) f =
                 list_iter2_aux (length l) (length [a]) default ([a] ++ l) (f a b) f). {
      simpl.
      reflexivity.
    }
    rewrite Heq; clear Heq.
    rewrite list_iter2_aux_equiv with (l2 := []).
    simpl.
    reflexivity.
Qed.

Lemma nth_error_split_intro:
  forall {A:Type} l1 (a: A) l,
    nth_error ((l1 ++ [a]) ++ l) (List.length l1) = Some a.
Proof.
  induction l1; simpl; intros.
  - f_equal.
  - rewrite IHl1.
    f_equal.
Qed.

Lemma skipn_nil_overflow:
  forall {A} n (l:list A),
  skipn n l = [] ->
    (List.length l <= n)%nat.
Proof.
  induction n; simpl; intros.
  - rewrite H.
    simpl.
    lia.
  - destruct l.
    + simpl; lia.
    + specialize (IHn _ H).
      simpl.
      lia.
Qed.

Lemma List32_assign_Some:
  forall l pc v
    (HMAX: pc < List.length l),
      exists l1, List32.assign' l pc v = Some l1.
Proof.
  induction l; simpl; intros.
  { apply Nat.nlt_0_r in HMAX.
    inversion HMAX.
  }

  destruct pc.
  {
    eexists; reflexivity.
  }

  assert (Heq: pc < Datatypes.length l) by lia.
  eapply IHl in Heq.
  destruct Heq as (l1 & Heq).
  rewrite Heq.
  eexists; reflexivity.
Qed.

Lemma List32_assign_same_length:
  forall l pc v l1
    (HA: List32.assign' l pc v = Some l1),
      List.length l = List.length l1.
Proof.
  induction l; simpl; intros.
  { inversion HA. }

  destruct pc.
  {
    inversion HA; subst.
    simpl.
    reflexivity.
  }

  destruct List32.assign' as [nl |] eqn: Ha; inversion HA; subst.
  eapply IHl in Ha.
  rewrite Ha.
  simpl; reflexivity.
Qed.

Lemma List32_assign_nth_error_other:
  forall l1 l2 ofs ofs' v
    (Hneq: ofs <> ofs')
    (HA: List32.assign' l1 ofs v = Some l2),
    List.nth_error l1 ofs' = List.nth_error l2 ofs'.
Proof.
  induction l1; simpl; intros.
  { inversion HA. }

  destruct ofs.
  - inversion HA; subst.
    destruct ofs'.
    + exfalso; apply Hneq; reflexivity.
    + simpl.
      reflexivity.
  - destruct List32.assign' as [nl |] eqn: Ha; inversion HA; subst; clear HA.
    destruct ofs'.
    + simpl; reflexivity.
    + simpl.
      eapply IHl1 in Ha; eauto.
Qed.

Lemma List32_assign_nth_error_same:
  forall l1 l2 ofs v
    (HA: List32.assign' l1 ofs v = Some l2),
    List.nth_error l2 ofs = Some v.
Proof.
  induction l1; simpl; intros.
  { inversion HA. }

  destruct ofs.
  - inversion HA; subst.
    simpl.
    reflexivity.
  - destruct List32.assign' as [nl |] eqn: Ha; inversion HA; subst; clear HA.
    simpl.
    eapply IHl1 in Ha; eauto.
Qed.